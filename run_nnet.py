import numpy
import os
import tensorflow as tf
import time

from eval import map_score, precision_score, get_map_score
from readerQA import load_data

import warnings
warnings.filterwarnings("ignore")  # TODO remove



def main():
    data_dir = 'mswiki'
    trainData, devData, testData, qids_dev, qids_test = load_data(data_dir)

    q_max_sent_size = trainData.dataset[0].shape[1]
    a_max_sent_size = trainData.dataset[1].shape[1]

    # Load word2vec embeddings
    fname = os.path.join(data_dir, 'emb_aquaint+wiki.txt.gz.ndim=50.bin.npy')
    print "Loading word embeddings from", fname
    vocab_emb = numpy.load(fname)
    ndim = vocab_emb.shape[1]
    dummpy_word_idx = numpy.max(trainData.dataset[1])
    print "Word embedding matrix size:", vocab_emb.shape

    # Create overlap embeddings
    overlapDim = 5
    print "Generating random vocabulary for word overlap indicator features with dim:", overlapDim
    dummy_word_id = numpy.max(trainData.dataset[3])
    print "Gaussian"
    numpy_rng = numpy.random.RandomState(seed=123)
    vocab_emb_overlap = numpy_rng.randn(dummy_word_id+1, overlapDim) * 0.25
    vocab_emb_overlap[-1] = 0

    # Configuration parameters
    n_classes = 2
    n_epochs = 100
    # batch_size = 25
    learning_rate = 0.1
    max_norm = 0
    dropout_rate = 0.5
    filterWidth = 5
    nkernels = 100
    param_stddev = 1.0

    # print 'batch_size', batch_size
    print 'n_epochs', n_epochs
    print 'learning_rate', learning_rate
    print 'max_norm', max_norm


    # Create input variables
    x_q = tf.placeholder(tf.int32, shape=[None, q_max_sent_size])
    x_a = tf.placeholder(tf.int32, shape=[None, a_max_sent_size])
    x_overlap_q = tf.placeholder(tf.int32, shape=[None, q_max_sent_size])
    x_overlap_a = tf.placeholder(tf.int32, shape=[None, a_max_sent_size])
    y_ = tf.placeholder(tf.int32, shape=[None])
    batch_size = tf.placeholder(dtype=tf.int32)


    def get_embeddings(input, paddings):
        embedding = tf.get_variable("embedding", vocab_emb.shape, trainable=False)
        return tf.pad(tf.nn.embedding_lookup(embedding, input), paddings, "CONSTANT")

    def get_overlap_embeddings(input, paddings):
        overlap_embedding = tf.get_variable("overlap", vocab_emb_overlap.shape)
        return tf.pad(tf.nn.embedding_lookup(overlap_embedding, input), paddings, "CONSTANT")

    paddings = [[0,0], [filterWidth-1, filterWidth-1], [0,0]]
    with tf.variable_scope("embedding_scope") as emb_scope:
        q_emb = tf.cast(get_embeddings(x_q, paddings), dtype=tf.float32)
        emb_scope.reuse_variables()
        a_emb = tf.cast(get_embeddings(x_a, paddings), dtype=tf.float32)

    with tf.variable_scope("overlap_embedding_scope") as overlap_emb_scope:
        q_overlap_emb = tf.cast(get_overlap_embeddings(x_overlap_q, paddings), dtype=tf.float32)
        overlap_emb_scope.reuse_variables()
        a_overlap_emb = tf.cast(get_overlap_embeddings(x_overlap_a, paddings), dtype=tf.float32)

    q_emb_concat = tf.concat(2, [q_emb, q_overlap_emb])
    a_emb_concat = tf.concat(2, [a_emb, a_overlap_emb])

    ndim += vocab_emb_overlap.shape[1]
    conv_space_Q = q_max_sent_size + 2*(filterWidth-1)
    conv_space_A = a_max_sent_size + 2*(filterWidth-1)

    # Define convolutional layer
    def conv_layer(input, filter_shape, nkernels, conv_space, stddev):
        W_conv = tf.get_variable("conv_W", filter_shape, initializer=tf.random_uniform_initializer(minval=-0.1, maxval=0.1))
        b_conv = tf.get_variable("conv_b", nkernels, initializer=tf.zeros_initializer)
        h_conv = tf.nn.tanh(tf.nn.conv2d(input, filter=W_conv, strides=[1, 1, ndim, 1], padding="SAME") + b_conv)
        h_pool = tf.nn.max_pool(h_conv, ksize=[1, conv_space, 1, 1], strides=[1, conv_space, 1, 1], padding='SAME')
        return tf.reshape(h_pool, [-1, nkernels])

    filter_shape = [filterWidth, ndim, 1, nkernels] # [height, width, in_channels, out_channels]
    q_emb_resized = tf.reshape(q_emb_concat, (-1, conv_space_Q, ndim, 1))
    a_emb_resized = tf.reshape(a_emb_concat, (-1, conv_space_A, ndim, 1))
    with tf.variable_scope("conv_scope") as conv_scope:
        h_pool_q = conv_layer(q_emb_resized, filter_shape, nkernels, conv_space_Q, param_stddev)
        conv_scope.reuse_variables()
        h_pool_a = conv_layer(a_emb_resized, filter_shape, nkernels, conv_space_A, param_stddev)


    # M = tf.Variable(tf.random_uniform([nkernels, nkernels], minval=-0.1, maxval=0.1))
    # dotProd = tf.matmul(tf.matmul(h_pool_q, M), tf.transpose(h_pool_a))
    # t = tf.sqrt(tf.size(dotProd))
    # diag = tf.diag_part(dotProd)
    # # concat = tf.concat(1, [h_pool_q, dotProd, h_pool_a])

    # Combine result
    sub = tf.abs(tf.sub(h_pool_q, h_pool_a))
    mul = tf.mul(h_pool_q, h_pool_a)
    concat = tf.concat(1, [sub, mul])

    # Fully connected linear layer (2*nkernels, 2*nkernels)
    h_w = tf.Variable(tf.random_uniform([2*nkernels, 2*nkernels], minval=-0.1, maxval=0.1), name="hidden_w")
    h_b = tf.Variable(tf.zeros([2*nkernels]), name="hidden_b")
    hid_out = tf.nn.tanh(tf.nn.xw_plus_b(concat, h_w, h_b), "hidden_out")

    # Output layer (2*nkernels, nclasses)
    output_w = tf.Variable(tf.random_uniform([2*nkernels, n_classes], minval=-0.1, maxval=0.1), name="output_w")
    output_b = tf.Variable(tf.zeros([n_classes]), name="output_b")
    y_conv = tf.nn.sigmoid(tf.nn.xw_plus_b(hid_out, output_w, output_b))
    loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(y_conv, y_))

    tf.scalar_summary(loss.op.name, loss)
    optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate)
    global_step = tf.Variable(0, name='global_step', trainable=False)
    train_op = optimizer.minimize(loss, global_step=global_step)
    prediction = tf.arg_max(tf.nn.softmax(y_conv), 1)

    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())

        best_dev_map = -numpy.inf
        best_test_map = -numpy.inf
        best_test_acc = -numpy.inf

        epoch = 0
        timer_train = time.time()
        no_best_dev_update = 0
        num_train_batches = trainData.size / 25
        saver = tf.train.Saver()

        while epoch < n_epochs:
            timer = time.time()
            i = 0
            tot_cost = 0
            count = 0
            while i < num_train_batches:
                batch = trainData.getNext(25)
                feed_dict = {x_q: batch[0], x_a: batch[1], x_overlap_q: batch[2], x_overlap_a : batch[3], y_: batch[4], batch_size: batch[0].shape[0]}
                _, cost = sess.run([train_op, loss], feed_dict=feed_dict)
                tot_cost += cost
                count += 1
                if i % 50 == 0 or i == num_train_batches:
                    y_pred_dev = prediction.eval(feed_dict={x_q: devData.dataset[0], x_a: devData.dataset[1], x_overlap_q: devData.dataset[2],
                                                            x_overlap_a: devData.dataset[3], y_: devData.dataset[4]})
                    dev_map = map_score(qids_dev, devData.dataset[4], y_pred_dev)
                    print('epoch: {} batch: {} train cost: {:.6f} '.format(epoch, i, tot_cost/count))
                    tot_cost = 0
                    count = 0
                    get_map_score(qids_dev, devData.dataset[4], y_pred_dev, './eval')

                    if dev_map > best_dev_map:
                        y_pred_test = prediction.eval(feed_dict={x_q: testData.dataset[0], x_a: testData.dataset[1], x_overlap_q: testData.dataset[2],
                                                                 x_overlap_a: testData.dataset[3], y_: testData.dataset[4]})
                        test_map = map_score(qids_test, testData.dataset[4], y_pred_test) * 100
                        test_acc = precision_score(qids_test, testData.dataset[4], y_pred_test)
                        print('epoch: {} batch: {} dev auc: {:.4f}; test map: {:.4f}; test acc: {:.4f};'.format(epoch, i, dev_map, test_map, test_acc))

                        best_dev_map = dev_map
                        save_path = saver.save(sess, "model.ckpt", global_step=global_step)
                        print("Model saved in file: %s" % save_path)
                        no_best_dev_update = 0
                i += 1

            if no_best_dev_update >= 10:
                print "Quitting after of no update of the best score on test set", no_best_dev_update
                break

            print('epoch {} took {:.4f} seconds'.format(epoch, time.time() - timer))
            epoch += 1
            no_best_dev_update += 1

        print('Training took: {:.4f} seconds'.format(time.time() - timer_train))
        # saver.resotre(sess, "model.ckpt")
        # test_acc = map_score(qids_test, y_test, y_pred_test) * 100
        # fname = os.path.join('./out', 'best_dev_params.epoch={:02d};batch={:05d};dev_acc={:.2f}.dat'.format(epoch, i, best_dev_acc))
        # numpy.savetxt(os.path.join('./out', 'test.epoch={:02d};batch={:05d};dev_acc={:.2f}.predictions.npy'.format(epoch, i, best_dev_acc)), y_pred_test)

        """
        # test_prec = precision_score(qids_test, y_test, predict_prob_batch(test_set_iterator))
        # print "Test precision : " + str(test_prec)
        """

if __name__ == '__main__':
  main()
