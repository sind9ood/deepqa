
import os
import random
import logging

from gensim import corpora
from os import listdir
import numpy as np

logging.basicConfig(level=logging.DEBUG)

def read_alist(path):
    """
    Looks like that:
    The method returns context in the given file.
    """
    alist = []
    for line in open(path):
        items = line.strip().split(' ')
        alist.append(items[3])
    return alist


def prepare_data(data_dir, embed_path, embed_size):
    """
    Collect text from file in the given data directory, save context in a file for the first time preparing the data.
    Load vocabulary from the collected text if the vocabulary file does not exist.
    :param data_dir: A directory path where containing training, validation and testing files.
    :return: Vocabulary.
    """

    context_path = os.path.join(data_dir, 'context.txt')
    vocab_path = os.path.join(data_dir, 'vocab.txt')

    # Collect all sentences appearing in the given data directory
    if os.path.exists(context_path):
        context_file = open(context_path, 'r')
        context_list = context_file.read().splitlines()
    else:
        logging.debug(" [*] Combining all contexts from {}".format(data_dir))
        context_list = get_all_context(data_dir)
        context_file = open(context_path, 'w+')
        context_file.write('\n'.join(context_list))
        logging.debug(" [*] Saving contexts to {}".format(context_path))

    # # Collect vocabularies
    # if os.path.exists(vocab_path):
    #     dictionary = corpora.Dictionary.load(vocab_path)
    # else:
    #     logging.debug(" [*] Vocabulary file does not exist, build a new vocabulary ...")
    #     dictionary = corpora.Dictionary(line.lower().split() for line in context_list)
    #     vocab_len = len(dictionary.token2id)
    #     dictionary.token2id['UNKNOWN'] = vocab_len
    #     dictionary.token2id['PADDING'] = vocab_len + 1
    #     logging.debug(" [*] Saving vocabulary to %s" % vocab_path)
    #     dictionary.save(vocab_path)  # store the dictionary, for future reference
    # return dictionary.token2id, dictionary.token2id.keys()
    token2id, vocab_emb = read_embedding(embed_path, embed_size)
    return token2id, np.array(vocab_emb)


def read_embedding(embed_path, embed_size):
    logging.info("Reading vocabulary and word embeddings from given word2vec file ...")
    token2id = {}
    vocab_emb = []
    with open(embed_path) as embed_file:
        lines = embed_file.read().splitlines()
        num = len(lines)
        for i in range(0, num):
            line = lines[i]
            items = line.strip().split(' ')
            if items[0] != '':
                token2id[items[0]] = i
                vocab_emb.append(items[1:])
    return token2id, vocab_emb


def prepare_word_embedding(fname):
    logging.debug(" [*] Loading word embeddings from %s " % fname)
    vocab_emb = np.load(fname)
    logging.debug(" [*] Embeeding matrix shape: {}".format(vocab_emb.shape))
    return vocab_emb


def prepare_overlap_embedding(overlap_features_num, overlap_dim, seed):
    logging.debug(" [*] Generating random embeddings for word overlap indicator features ({}) with dimension: {}".format(overlap_features_num, overlap_dim))
    # numpy_rng = np.random.seed(seed)
    # vocab_emb_overlap = numpy_rng.randn(overlap_features_num, overlap_dim) * 0.25
    # vocab_emb_overlap[-1] = 0
    # return vocab_emb_overlap
    return np.random.normal(0, 1, (overlap_features_num, overlap_dim))


def get_all_context_insuranceQA(data_dir):
    """
    Collect plain text file from the given file.
    Data format is download from https://github.com/white127/insuranceQA-cnn-lstm/tree/master/insuranceQA
    """
    # Get context_list from training/validation/testing file.
    files = [os.path.join(data_dir, f) for f in listdir(data_dir) if "train" in f or "test" in f]
    context_list = []
    for file in files:
        for line in open(file):
            line = line[0:line.find("_<a>_") + 1]
            items = line.strip().split(' ')
            for i in range(2, 3):
                context = items[i].replace('_', ' ')
                if context not in context_list:
                    context_list.append(context)
    return context_list


def get_all_context(data_dir):
    """
    Collect context from files in the given directory.
    Format: IPSoft QA format.
    """
    files = [os.path.join(data_dir, f) for f in listdir(data_dir) if os.path.isfile(os.path.join(data_dir, f))]

    context_list = []
    for file in files:
        logging.debug(" [*] Processing {}".format(file))
        for line in open(file):
            items = line.strip().split('\t')
            for i in range(1, 3):
                context = items[i]
                if context not in context_list:
                    context_list.append(context)
    return context_list


def load_pos_neg_maps(file_name):
    """
    Load {question: positive_answers_list} and {question: negative_answers_list}
    """
    pos_dict = {}
    neg_dict = {}
    for line in open(file_name):
        items = line.strip().split('\t')
        label = items[0]
        que = items[1]
        sen = items[2]
        if label == "1":
            if que in pos_dict:
                pos_dict.update({que: pos_dict[que] + [sen]})
            else:
                pos_dict[que] = [sen]
        else:
            if que in neg_dict:
                neg_dict.update({que: neg_dict[que] + [sen]})
            else:
                neg_dict[que] = [sen]
    return pos_dict, neg_dict


def encode_sen(vocab, input, sequence_size):
    x = []
    words = input.strip().split(' ')
    for w in words:
        if w in vocab:
            x.append(vocab[w])
        else:
            x.append(vocab['UNKNOWN'])
    x = x + [vocab['PADDING'] for i in range(len(words), sequence_size)]
    return x


def load_data_batched(file_name, vocab, sequence_size, batch_size):
    """
    Randomly select a training instance. For one question, keep one correct sentence,
    and randomly select an incorrect sentence.
    """
    x_train_1 = []
    x_train_2 = []
    x_train_3 = []
    pos_dict, neg_dict = load_pos_neg_maps(file_name)
    keys = list(pos_dict.keys())

    for i in range(0, batch_size):
        que = keys[random.randint(0, len(keys) - 1)]
        pos_ans = pos_dict[que][0]
        neg_ans = neg_dict[que]
        x_train_1.append(encode_sen(vocab, que, sequence_size))
        x_train_2.append(encode_sen(vocab, pos_ans, sequence_size))
        # randomly select the negative anser
        neg_idx = random.randint(0, len(neg_ans) - 1)
        x_train_3.append(encode_sen(vocab, list(neg_ans)[neg_idx], sequence_size))

    return np.array(x_train_1), np.array(x_train_2), np.array(x_train_3)


def load_test(file_name):
    """
    Load data from given file_name.
    :param file_name: path to data file.
    :return: a list of [golden_label, question, sentence]
    """
    x_test = []
    for line in open(file_name):
        x_test.append(line.strip().split('\t'))
    return x_test


def load_data(file_name, vocab, sequence_size, pos_neg):
    """
    For each question, keep one positive sentence, the negative sentence is set to be same as positive.
    """
    pos_dict, neg_dict = load_pos_neg_maps(file_name)
    x_data_1 = []
    x_data_2 = []
    x_data_3 = []
    ques = []
    sens = []

    for line in open(file_name):
        items = line.strip().split('\t')
        label = items[0]
        if (pos_neg and label == "1") or (not pos_neg):
            que = items[1]
            sen = items[2]
            que_encoded = encode_sen(vocab, que, sequence_size)
            sen_encoded = encode_sen(vocab, sen, sequence_size)
            x_data_1.append(que_encoded)
            x_data_2.append(sen_encoded)
            if pos_neg:
                neg_ans = neg_dict[que]
                neg_idx = random.randint(0, len(neg_ans) - 1)
                x_data_3.append(encode_sen(vocab, list(neg_ans)[neg_idx], sequence_size))
            else:
                x_data_3.append(sen_encoded)
            ques.append(que)
            sens.append(sen)

    return np.array(x_data_1), np.array(x_data_2), np.array(x_data_3), ques, sens, pos_dict


def get_batched_eval_data(filename, vocab, sequence_size, batch_size, pos_neg):
    """
    Load data which is divided by batch_size. x_batched_1 is the representation of question;
    x_batched_2 is the representation of correct sentence; x_batched_3, in this case, is the
    same as x_batched_2 since this is a testing stage, not learning stage.
    :param filename: path to data file
    :param vocab: vocabulary
    :param sequence_size: predefined maximum sequence size
    :param batch_size: predefined batch size
    :return: (1) [(t1, t2, t3, t4, t5)], where t1 is a list of encoded questions; t2 is
    the corresponding list of encoded correct sentences; t3 is the same as t2 in this case;
    t4 is a list of questions; t5 is a list of correct sentences. (2) number of total instances.
    (3) number of distinct questions.
    """
    data_1, data_2, data_3, ques, sens, que_pos = load_data(filename, vocab, sequence_size, pos_neg)
    total_data_size = len(data_1)
    batched_data_list = []
    start_index = int(0)
    stop = False
    while not stop:
        x_batched_1 = []
        x_batched_2 = []
        x_batched_3 = []
        que_batched = []
        sen_batched = []
        for i in range(0, batch_size):
            true_index = start_index + i
            if true_index >= total_data_size:
                stop = True
                break
            x_batched_1.append(data_1[true_index])
            x_batched_2.append(data_2[true_index])
            x_batched_3.append(data_3[true_index])
            que_batched.append(ques[true_index])
            sen_batched.append(sens[true_index])

        current_batch_size = len(x_batched_1)
        if 0 < current_batch_size < batch_size:
            for i in range(current_batch_size, batch_size):
                x_batched_1.append(x_batched_1[0])
                x_batched_2.append(x_batched_2[0])
                x_batched_3.append(x_batched_3[0])
                que_batched.append(que_batched[0])
                sen_batched.append(sen_batched[0])

        batched_data_list.append((x_batched_1, x_batched_2, x_batched_3, que_batched, sen_batched))
        start_index += batch_size
    return batched_data_list, que_pos, len(data_1), len(que_pos)


def convert_to_input(batched, batch_size):
    x_batched_1 = []
    x_batched_2 = []
    x_batched_3 = []
    que_batched = []
    cand_batched = []
    for i in range(0, batch_size):
        x_batched_1.append(batched[0][i])
        x_batched_2.append(batched[1][i])
        x_batched_3.append(batched[2][i])
        que_batched.append(batched[3][i])
        cand_batched.append(batched[4][i])
    return x_batched_1, x_batched_2, x_batched_3, que_batched, cand_batched


def load_data_val(testList, index, vocab, sequence_size, batch_size):
    x_data_1 = []
    x_data_2 = []
    x_data_3 = []
    for i in range(0, batch_size):
        true_index = index + 1
        if (true_index >= len(testList)):
            true_index = len(testList) - 1
        items = testList[true_index]
        x_data_1.append(encode_sen(vocab, items[1], sequence_size))
        x_data_2.append(encode_sen(vocab, items[2], sequence_size))
        x_data_3.append(encode_sen(vocab, items[2], sequence_size))
    return np.array(x_data_1), np.array(x_data_2), np.array(x_data_3)
