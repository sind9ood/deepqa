
import numpy as np
import random

class QaCorpus(object):
    def __init__(self, file_path, shuffle):
        self._file_path = file_path
        self.label_list = []
        self.que_tokens_list = []
        self.sen_tokens_list = []
        self.label_indexed_list = []
        self.que_tokens_indexed_list = []
        self.sen_tokens_indexed_list = []
        self.que_overlap_list = []
        self.sen_overlap_list = []
        self.que_max_seq_size = -np.inf
        self.sen_max_seq_size = -np.inf
        self.num_instances = 0
        self.shuffle = shuffle


        self.que_matrix = None
        self.sen_matrix = None
        self.que_overlap_matrix = None
        self.sen_overlap_matrix = None
        self.y_matrix = None


    def __iter__(self):
        for line in open(self._file_path):
            items = line.strip().lower().split('\t')
            yield [items[0], items[1].split(' '), items[2].split(' ')]

    def add(self, label, que_tokens, sen_tokens, label_indexed,
            que_tokens_indexed, sen_tokens_indexed,
            que_overlap, sen_overlap):


        print(len(sen_tokens_indexed))



        que_len = len(que_tokens_indexed)
        if que_len > self.que_max_seq_size:
            self.que_max_seq_size = que_len
        sen_len = len(sen_tokens_indexed)
        if sen_len > self.sen_max_seq_size:
            self.sen_max_seq_size = sen_len

        self.label_list.append(label)
        self.que_tokens_list.append(que_tokens)
        self.sen_tokens_list.append(sen_tokens)
        self.label_indexed_list.append(label_indexed)
        self.que_tokens_indexed_list.append(que_tokens_indexed)
        self.sen_tokens_indexed_list.append(sen_tokens_indexed)
        self.que_overlap_list.append(que_overlap)
        self.sen_overlap_list.append(sen_overlap)
        self.num_instances += 1

    def normalize(self, padding_idx, que_max_seq_size, sen_max_seq_size):
        for i in range(0, self.num_instances):
            que_missing_number = que_max_seq_size - len(self.que_tokens_indexed_list[i])
            self.que_tokens_indexed_list[i] += [padding_idx] * que_missing_number
            self.que_overlap_list[i] += [2] * que_missing_number

            sen_missing_number = sen_max_seq_size - len(self.sen_tokens_indexed_list[i])
            self.sen_tokens_indexed_list[i] += [padding_idx] * sen_missing_number
            self.sen_overlap_list[i] += [2] * sen_missing_number

        # for i in range(0, self.num_instances):
        #     que_missing_number = que_max_seq_size - len(self.que_tokens_indexed_list[i])
        #     que_missing_left = que_missing_number / 2
        #     que_missing_right = que_missing_number - que_missing_left
        #     padding_que_left = [padding_idx] * que_missing_left
        #     padding_que_right = [padding_idx] * que_missing_right
        #     padding_overlap_que_left = [2] * que_missing_left
        #     padding_overlap_que_right = [2] * que_missing_right
        #
        #     sen_missing_number = sen_max_seq_size - len(self.sen_tokens_indexed_list[i])
        #     sen_missing_left = sen_missing_number / 2
        #     sen_missing_right = sen_missing_number - sen_missing_left
        #     padding_sen_left = [padding_idx] * sen_missing_left
        #     padding_sen_right = [padding_idx] * sen_missing_right
        #     padding_overlap_sen_left = [2] * sen_missing_left
        #     padding_overlap_sen_right = [2] * sen_missing_right
        #
        #
        #     self.que_tokens_indexed_list[i] = padding_que_left + self.que_tokens_indexed_list[i] + padding_que_right
        #     self.que_overlap_list[i] = padding_overlap_que_left + self.que_overlap_list[i] + padding_overlap_que_right
        #
        #     self.sen_tokens_indexed_list[i] = padding_sen_left + self.sen_tokens_indexed_list[i] + padding_sen_right
        #     self.sen_overlap_list[i] = padding_overlap_sen_left + self.sen_overlap_list[i] + padding_overlap_sen_right

        self.que_matrix = np.array(self.que_tokens_indexed_list, dtype=object)
        self.sen_matrix = np.array(self.sen_tokens_indexed_list, dtype=object)
        self.que_overlap_matrix = np.array(self.que_overlap_list, dtype=object)
        self.sen_overlap_matrix = np.array(self.sen_overlap_list, dtype=object)
        self.y_matrix = np.array(self.label_indexed_list, dtype=object)

    def get_batches(self, batch_size, shuffle):
        que_batch_list = []
        sen_batch_list = []
        que_overlap_batch_list = []
        sen_overlap_batch_list = []
        label_batch_list = []

        if shuffle:
            random_indices = random.sample(range(0, self.num_instances), self.num_instances)
        else:
            random_indices = [i for i in range(0, self.num_instances)]
        i = 0
        # print("OGHHHJJKJFKJAKFJFKL   \t\t\t\t{}".format(shuffle))
        # print(random_indices)
        while i < len(random_indices):
            if i+batch_size > self.num_instances:
                break
            que_batch = []
            sen_batch = []
            que_overlap_batch = []
            sen_overlap_batch = []
            label_batch = []
            used = []
            for k in range(i, i+batch_size):
                true_idx = random_indices[k]

                # print(true_idx)
                # if true_idx in used:
                #     print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ERRIR")
                # used.append(true_idx)


                que_batch.append(self.que_matrix[true_idx])
                sen_batch.append(self.sen_matrix[true_idx])
                que_overlap_batch.append(self.que_overlap_matrix[true_idx])
                sen_overlap_batch.append(self.sen_overlap_matrix[true_idx])
                label_batch.append(self.label_indexed_list[true_idx])
            que_batch_list.append(np.array(que_batch))
            sen_batch_list.append(np.array(sen_batch))
            que_overlap_batch_list.append(np.array(que_overlap_batch))
            sen_overlap_batch_list.append(np.array(sen_overlap_batch))
            label_batch_list.append(label_batch)
            i += batch_size
        return (que_batch_list, sen_batch_list, que_overlap_batch_list, sen_overlap_batch_list, label_batch_list)
