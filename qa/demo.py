# import os
# import sys
# import time
# import logging
# import datetime
# import operator
#
# import numpy as np
# import tensorflow as tf
#
# import data_utils
# import data_reader
# from cnn_qa import CnnQaModel
# from qa_classifier_model import CnnQaClassifierModel
#
# logging.basicConfig(level=logging.DEBUG)
#
# with open("senna.txt") as embed_file:
#     token2id = {}
#     vocab_emb = []
#     lines = embed_file.read().splitlines()
#     num = len(lines)
#     for i in range(0, num):
#         line = lines[i]
#         items = line.strip().split(' ')
#         token2id[items[0]] = i
#         vocab_emb.append(items[1:])
#
#     print(token2id)
#     print(vocab_emb[0:10])
#     print(len(token2id))
#     print(len(vocab_emb))
#
#
#
#
#


import os
import sys
import time
import logging
import datetime
import operator

import numpy as np
import tensorflow as tf

import data_utils
import data_reader
from cnn_qa import CnnQaModel
from qa_classifier_model import CnnQaClassifierModel

logging.basicConfig(level=logging.DEBUG)

# tf.flags.DEFINE_integer("sequence_size", 200, "Predfined the size of each sequence (default: 200)")
tf.flags.DEFINE_integer("embedding_dim", 50, "Dimensionality of character embedding. ")
tf.flags.DEFINE_integer("overlap_dim", 5, "Dimensionality of overlapping features. ")
tf.flags.DEFINE_integer("filter_size", 5, "Filter size (default = 5)")
tf.flags.DEFINE_integer("n_filters", 100, "Number of filters (default = 100)")
tf.flags.DEFINE_integer("n_classes", 2, "Number of classes (default = 2)")
tf.flags.DEFINE_integer("stopping_criterion", 10, "Predefined stopping criterion in training (default: 10)")
tf.flags.DEFINE_integer("num_epoches", 999, "Training maximum iterations (default = 100)")
tf.flags.DEFINE_float("dropout_in", 0.5, "Dropout probability for input layer (default = 0.2)")
tf.flags.DEFINE_float("dropout_hid", 0.5, "Dropout probability for hidden layer (default = 0.3)")
# tf.flags.DEFINE_float("dropout_keep_prob", 1.0, "Dropout keep probability. ")
tf.flags.DEFINE_float("learning_rate", 0.0001, "Learning rate (default = 0.0001)")
tf.flags.DEFINE_float("param_stddev", 0.1, "TODO?")
tf.flags.DEFINE_float("l2", 0, "L2 regularization weight")
tf.flags.DEFINE_string("dataset", "insurancewiki", "dataset name (default is insurancewiki)")
tf.flags.DEFINE_string("output_dir", "runs", "Directory for saving outputs")
tf.flags.DEFINE_string("data_dir", "/Users/qingqingcai/Documents/corpus/deepqa", "Data directory to training, validation and testing files")
tf.flags.DEFINE_string("embedding_name", "emb_aquaint+wiki.txt.gz.ndim=50.bin.npy", "Pretrained word embedding file")
tf.flags.DEFINE_string("embed_path", "senna.txt", "Pretrained word embedding file")
tf.flags.DEFINE_boolean("test", False, "Evaluate on specific data directory")
tf.flags.DEFINE_boolean("iterative_test", False, "Iterative test based on users' inputs")

# Training parameters
tf.flags.DEFINE_integer("batch_size", 100, "Batch Size (default: 64)")
tf.flags.DEFINE_integer("evaluate_every", 10, "Evaluate model on dev set after this many steps (default: 100)")
tf.flags.DEFINE_integer("checkpoint_every", 10, "Save model after this many steps (default: 100)")

# Misc Parameters
tf.flags.DEFINE_boolean("allow_soft_placement", True, "Allow device soft device placement")
tf.flags.DEFINE_boolean("log_device_placement", False, "Log placement of ops on devices")
tf.flags.DEFINE_boolean("shuffle", True, "Allow shuffle in data (default = True)")

FLAGS = tf.flags.FLAGS
FLAGS._parse_flags()
logging.info("\n Parameters:")
for attr, value in sorted(FLAGS.__flags.items()):
    logging.info(" [*] {} = {}".format(attr.upper(), value))
logging.info("")

# Prepare Parameters and Vocabulary
# ==============================================================
data_dir = os.path.join(FLAGS.data_dir, FLAGS.dataset)
w2v_path = os.path.join(os.curdir, FLAGS.embedding_name)
train_file = os.path.join(data_dir, "train.txt")
valid_file = os.path.join(data_dir, "dev.txt")
test_file = os.path.join(data_dir, "test.txt")

output_dir = os.path.join(FLAGS.output_dir, FLAGS.dataset)
checkpoint_dir = os.path.abspath(os.path.join(output_dir, "checkpoints"))
checkpoint_prefix = os.path.join(checkpoint_dir, "model")

logging.info(" [*] Starting loading data")
# vocab, _ = data_utils.prepare_data(data_dir)
# vocab_emb = data_utils.prepare_word_embedding(w2v_path)
vocab, vocab_emb = data_utils.prepare_data(data_dir, FLAGS.embed_path, FLAGS.embedding_dim)
vocab_emb_overlap_que = data_utils.prepare_overlap_embedding(3, FLAGS.overlap_dim, 123)
vocab_emb_overlap_ans = data_utils.prepare_overlap_embedding(3, FLAGS.overlap_dim, 123)

# Load Data
# ==============================================================
train_corpus, dev_corpus, test_corpus = data_reader.load_data(data_dir, vocab, FLAGS.shuffle)
logging.info(" [*] Loaded {} train instances".format(train_corpus.num_instances))
logging.info(" [*] Loaded {} dev instances".format(dev_corpus.num_instances))
logging.info(" [*] Loaded {} test instances".format(test_corpus.num_instances))

# TODO: how to decide the maximum size
que_max_sent_size = max(train_corpus.que_max_seq_size, dev_corpus.que_max_seq_size)
sen_max_sent_size = max(train_corpus.sen_max_seq_size, dev_corpus.sen_max_seq_size)


