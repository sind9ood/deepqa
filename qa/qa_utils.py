
import tensorflow as tf

def get_embeddings(input, embedding, paddings):
    """
    Get word embeddings for each word in the given input, with given paddings abd pre-defined embed_shape.
    """
    # return tf.pad(tf.nn.embedding_lookup(embedding, input), paddings, "CONSTANT")
    return tf.nn.embedding_lookup(embedding, input)





