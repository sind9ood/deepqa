from qa_corpus import QaCorpus
import os
import random
import logging

from gensim import corpora
from os import listdir
import numpy as np

logging.basicConfig(level=logging.DEBUG)

def load_vocabulary(data_dir):
    """
    Collect text from file in the given data directory, save context in a file for the first time preparing the data.
    Load vocabulary from the collected text if the vocabulary file does not exist.
    :param data_dir: A directory path where containing training, validation and testing files.
    :return: Vocabulary.
    """

    context_path = os.path.join(data_dir, 'context.txt')
    vocab_path = os.path.join(data_dir, 'vocab.txt')

    # Collect all sentences appearing in the given data directory
    if os.path.exists(context_path):
        context_file = open(context_path, 'r')
        context_list = context_file.read().splitlines()
    else:
        logging.info(" [*] Combining all contexts from {}".format(data_dir))
        context_list = get_all_context(data_dir)
        context_file = open(context_path, 'w+')
        context_file.write('\n'.join(context_list))
        logging.info(" [*] Saving contexts to {}".format(context_path))

    # Collect vocabularies
    if os.path.exists(vocab_path):
        dictionary = corpora.Dictionary.load(vocab_path)
    else:
        logging.info(" [*] Vocabulary file does not exist, build a new vocabulary ...")
        dictionary = corpora.Dictionary(line.lower().split() for line in context_list)
        vocab_len = len(dictionary.token2id)
        dictionary.token2id['UNKNOWN'] = vocab_len
        dictionary.token2id['PADDING'] = vocab_len + 1
        logging.info(" [*] Saving vocabulary to %s" % vocab_path)
        dictionary.save(vocab_path)  # store the dictionary, for future reference
    return dictionary.token2id, dictionary.token2id.keys()


def get_all_context(data_dir):
    """
    Collect context from files in the given directory.
    Format: IPSoft QA format.
    """
    files = [os.path.join(data_dir, f) for f in listdir(data_dir) if os.path.isfile(os.path.join(data_dir, f))]

    context_list = []
    for file in files:
        logging.info(" [*] Processing {}".format(file))
        for line in open(file):
            items = line.strip().split('\t')
            for i in range(1, 3):
                context = items[i]
                if context not in context_list:
                    context_list.append(context)
    return context_list


def load_file(file_path, vocab, shuffle):
    corpus = QaCorpus(file_path, shuffle)
    for label, que_tokens, sen_tokens in corpus:

        label_indexed = []
        label_indexed.append(0 if label == '0' else 1)
        label_indexed.append(1 if label == '0' else 0)

        que_tokens_indexed = []
        sen_tokens_indexed = []
        for token in que_tokens:
            if token not in vocab:
                que_tokens_indexed.append(vocab['UNKNOWN'])
            elif token is not '':
                que_tokens_indexed.append(vocab[token])
        for token in sen_tokens:
            if token not in vocab:
                sen_tokens_indexed.append(vocab['UNKNOWN'])
            elif token is not '':
                sen_tokens_indexed.append(vocab[token])
        que_overlap, sen_overlap = get_overlap(que_tokens_indexed, sen_tokens_indexed)
        corpus.add(label, que_tokens, sen_tokens, label_indexed,
                   que_tokens_indexed, sen_tokens_indexed,
                   que_overlap, sen_overlap)
    return corpus


def get_overlap(que_tokens_indexed, sen_tokens_indexed):
    """
    data_indexed is defined as a list of tuples. For example,
    data_indexed[i][0] = label_indexed
    data_indexed[i][1] = que_tokens_indexed
    data_indexed[i][1] = sen_tokens_indexed
    :param data_indexed:
    :return: que_overlap, sen_overlap
    """
    que_overlap = [1 if token_id in sen_tokens_indexed else 0 for token_id in que_tokens_indexed]
    sen_overlap = [1 if token_id in que_tokens_indexed else 0 for token_id in sen_tokens_indexed]
    return que_overlap, sen_overlap


def load_data(file_dir, vocab, shuffle):
    file_train = os.path.join(file_dir, "train.txt")
    file_dev = os.path.join(file_dir, "dev.txt")
    file_test = os.path.join(file_dir, "test.txt")

    train_corpus = load_file(file_train, vocab, shuffle)
    dev_corpus = load_file(file_dev, vocab, shuffle)
    test_corpus = load_file(file_test, vocab, shuffle)

    padding_idx = vocab['PADDING']
    # que_max_sent_size = max(train_corpus.que_max_seq_size, dev_corpus.que_max_seq_size)
    sen_max_sent_size = max(train_corpus.sen_max_seq_size, dev_corpus.sen_max_seq_size)
    que_max_sent_size = sen_max_sent_size
    train_corpus.normalize(padding_idx, que_max_sent_size, sen_max_sent_size)
    dev_corpus.normalize(padding_idx, que_max_sent_size, sen_max_sent_size)
    test_corpus.normalize(padding_idx, que_max_sent_size, sen_max_sent_size)

    return train_corpus, dev_corpus, test_corpus


