
import os
import sys
import time
import logging
import datetime
import operator

import numpy as np
import tensorflow as tf

import data_utils
from cnn_qa import CnnQaModel

logging.basicConfig(level=logging.DEBUG)

tf.flags.DEFINE_integer("sequence_size", 200, "Predfined the size of each sequence (default: 200)")
tf.flags.DEFINE_integer("embed_dim", 50, "Dimensionality of character embedding. ")
tf.flags.DEFINE_integer("num_filters", 500, "Number of filters per filter size. ")
tf.flags.DEFINE_integer("stopping_criterion", 10, "Predefined stopping criterion in training (default: 10)")
tf.flags.DEFINE_float("dropout_keep_prob", 1.0, "Dropout keep probability. ")
tf.flags.DEFINE_float("l2_reg_lambda", 0, "L2 regularization lambda. ")
tf.flags.DEFINE_string("embed_path", "senna.txt", "Pretrained word embedding file")
tf.flags.DEFINE_string("dataset", "insurancewiki", "dataset name (default is insurancewiki)")
tf.flags.DEFINE_string("filter_sizes", "1,2,3,5", "Comma-separated filter sizes")
tf.flags.DEFINE_string("output_dir", "runs", "Directory for saving outputs")
tf.flags.DEFINE_string("data_dir", "/Users/qingqingcai/Documents/corpus/deepqa", "Data directory to training, validation and testing files")
tf.flags.DEFINE_boolean("test", False, "Evaluate on specific data directory")
tf.flags.DEFINE_boolean("iterative_test", False, "Iterative test based on users' inputs")

# Training parameters
tf.flags.DEFINE_integer("batch_size", 100, "Batch Size (default: 64)")
tf.flags.DEFINE_integer("num_epochs", 5000000, "Number of training epochs (default: 200)")
tf.flags.DEFINE_integer("evaluate_every", 10, "Evaluate model on dev set after this many steps (default: 100)")
tf.flags.DEFINE_integer("checkpoint_every", 10, "Save model after this many steps (default: 100)")

# Misc Parameters
tf.flags.DEFINE_boolean("allow_soft_placement", True, "Allow device soft device placement")
tf.flags.DEFINE_boolean("log_device_placement", False, "Log placement of ops on devices")

FLAGS = tf.flags.FLAGS
FLAGS._parse_flags()
logging.debug("\n Parameters:")
for attr, value in sorted(FLAGS.__flags.items()):
    logging.debug(" [*] {} = {}".format(attr.upper(), value))
logging.debug("")

# Prepare Parameters and Vocabulary
# ==============================================================
logging.debug(" [*] Starting loading data")
data_dir = os.path.join(FLAGS.data_dir, FLAGS.dataset)
vocab, embeddings = data_utils.prepare_data(data_dir, FLAGS.embed_path, FLAGS.embed_dim)
train_file = os.path.join(data_dir, "train.txt")
valid_file = os.path.join(data_dir, "dev.txt")
test_file = os.path.join(data_dir, "test.txt")

output_dir = os.path.join(FLAGS.output_dir, FLAGS.dataset)
checkpoint_dir = os.path.abspath(os.path.join(output_dir, "checkpoints"))
checkpoint_prefix = os.path.join(checkpoint_dir, "model")


# Load Data
# ==============================================================
testList = data_utils.load_test(test_file)
batched_train, que_pos_train, num_train_instances, num_train_distinct_ques = \
    data_utils.get_batched_eval_data(train_file, vocab, FLAGS.sequence_size, FLAGS.batch_size, True)
logging.debug(" [*] Loaded {} train instances, {} distinct questions.".format(num_train_instances, num_train_distinct_ques))

batched_dev, que_pos_dev, num_dev_instances, num_dev_distinct_ques = \
    data_utils.get_batched_eval_data(valid_file, vocab, FLAGS.sequence_size, FLAGS.batch_size, False)
logging.debug(" [*] Loaded {} dev instances, {} distinct questions.".format(num_dev_instances, num_dev_distinct_ques))
batched_test, que_pos_test, num_test_instances, num_test_distinct_ques = \
    data_utils.get_batched_eval_data(test_file, vocab, FLAGS.sequence_size, FLAGS.batch_size, False)
logging.debug(" [*] Loaded {} test instances, {} distinct questions.".format(num_test_instances, num_test_distinct_ques))


def create_model(session):
    model = CnnQaModel(sequence_length=FLAGS.sequence_size,
                       batch_size=FLAGS.batch_size,
                       vocab_size=len(vocab),
                       embeddings=embeddings,
                       embedding_size=FLAGS.embed_dim,
                       filter_sizes=list(map(int, FLAGS.filter_sizes.split(","))),
                       num_filters=FLAGS.num_filters,
                       l2_reg_lambda=FLAGS.l2_reg_lambda)
    ckpt = tf.train.get_checkpoint_state(checkpoint_dir)

    if (FLAGS.test or FLAGS.iterative_test) and ckpt and tf.gfile.Exists(ckpt.model_checkpoint_path):
        logging.debug(" [*] Reading model parameters from %s" % ckpt.model_checkpoint_path)
        model.saver.restore(session, ckpt.model_checkpoint_path)
    else:
        logging.debug(" [*] Created model from fresh parameters")

    return model


def compute_score(sess, model, batched_data_list, que_pos):
    """
    Given data and model, compute accuracy = corrected_predicted / total_examples.
    :param sess: tensorflow session
    :param model: trained/loaded model
    :param batched_data_list: batched training data
    :param que_pos: a dictionary from question to a list of correct (golden) sentences
    :return: accuracy score, defined by corrected_prediction / total_examples.
    """
    predicted_results = {}
    for batched in batched_data_list:
        x_batched_1, x_batched_2, x_batched_3, que_batched, cand_batched = \
            data_utils.convert_to_input(batched, FLAGS.batch_size)

        feed_dict = {
            model.input_x_1: np.array(x_batched_1),
            model.input_x_2: np.array(x_batched_2),
            model.input_x_3: np.array(x_batched_3),
            model.dropout_keep_prob: 1.0
        }

        batched_scores = sess.run([model.cos_12], feed_dict)
        for i in range(0, FLAGS.batch_size):
            que = que_batched[i]
            cand = cand_batched[i]
            score = batched_scores[0][i]
            if que in predicted_results:
                values = predicted_results[que]
                values[cand] = score
                predicted_results[que] = values
            else:
                predicted_results[que] = {cand: score}

    # print("predicted_results.size = %d" % len(predicted_results))
    correct = 0
    total = 0
    for que in predicted_results:
        cands_scores_sorted = sorted(predicted_results[que].items(), key=operator.itemgetter(1), reverse=True)
        predicted_ans = cands_scores_sorted[0][0]
        # print("")
        # print("que = %s" % que)
        # print("predicted = %s" % predicted_ans)
        # for exp in que_pos[que]:
        #    print("expected = %s " % exp)
        if predicted_ans in que_pos[que]:
            correct += 1
        elif FLAGS.test:
            logging.debug("")
            logging.debug("que = %s" % que)
            logging.debug("expected = %s" % que_pos[que])
            logging.debug("actual = %s" % predicted_ans)
        total += 1
        # for cand, score in cands_scores_sorted:
        #     print("\t{} = {}".format(score, cand))
    accuracy = (correct * 1.0) / (total * 1.0)
    # todo: compute map score by running trec_eval
    map_score = accuracy
    logging.debug(" precision = {} / {} = {}".format(correct, total, accuracy))
    return map_score


# Training
# ==============================================================
def train():
    # preparing data

    session_conf = tf.ConfigProto(allow_soft_placement=FLAGS.allow_soft_placement,
                                  log_device_placement=FLAGS.log_device_placement)
    sess = tf.Session(config=session_conf)
    with sess.as_default():
        # Create model
        model = create_model(sess)

        # Define training procedure
        global_step = tf.Variable(0, name="global_step", trainable=False)
        optimizer = tf.train.AdamOptimizer(1e-1)
        grads_and_vars = optimizer.compute_gradients(model.loss)
        train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

        # Keep track of gradient values and sparsity (optional)
        grad_summaries = []
        for g, v in grads_and_vars:
            if g is not None:
                grad_hist_summary = tf.histogram_summary("{}/grad/hist".format(v.name), g)
                grad_summaries.append(grad_hist_summary)
        grad_summaries_merged = tf.merge_summary(grad_summaries)

        # Output directory for models and summaries
        timestamp = str(int(time.time()))
        out_dir = os.path.join(output_dir, timestamp)
        logging.debug(" [*] Writing summaries to %s" % out_dir)

        # Summaries for loss and accuracy
        loss_summary = tf.scalar_summary("loss", model.loss)
        acc_summary = tf.scalar_summary("accuracy", model.accuracy)

        # Train summaries
        train_summary_op = tf.merge_summary([loss_summary, acc_summary, grad_summaries_merged])
        train_summary_dir = os.path.join(out_dir, "summaries", "train")
        train_summary_writer = tf.train.SummaryWriter(train_summary_dir, sess.graph_def)

        # Validation summaries
        dev_summary_op = tf.merge_summary([loss_summary, acc_summary])
        dev_summary_dir = os.path.join(out_dir, "summaries", "dev")
        dev_summary_writer = tf.train.SummaryWriter(dev_summary_dir, sess.graph_def)

        # Checkpoint directory
        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)

        model.saver = tf.train.Saver(tf.all_variables())
        logging.debug(" [*] Graph built finished.")

        # Initialize all variables
        sess.run(tf.initialize_all_variables())

        # DEFINE TRAINING STEP
        def train_step(x_batch_1, x_batch_2, x_batch_3):
            """A single training step"""
            feed_dict = {
                model.input_x_1: x_batch_1,
                model.input_x_2: x_batch_2,
                model.input_x_3: x_batch_3,
                model.dropout_keep_prob: FLAGS.dropout_keep_prob
            }
            _, step, summaries, loss, accuracy = sess.run(
                    [train_op, global_step, train_summary_op, model.loss, model.accuracy], feed_dict)
            time_str = datetime.datetime.now().isoformat()
            logging.debug("{}: step{}, loss: {:g}, acc {:g}".format(time_str, step, loss, accuracy))
            train_summary_writer.add_summary(summaries, step)
            return loss, accuracy

        # Generate batches and Training loop for each batch ...
        dev_max_map = -1.0
        iteration_no_change = 0

        for epoch in range(FLAGS.num_epochs):
            try:
                # x_batch_1, x_batch_2, x_batch_3 = \
                #     data_utils.load_data_batched(train_file, vocab, FLAGS.sequence_size, FLAGS.batch_size)

                total_loss = 0.0
                for batched_data in batched_train:
                    train_batch_1, train_batch_2, train_batch_3, _, _ = \
                        data_utils.convert_to_input(batched_data, FLAGS.batch_size)
                    loss, _ = train_step(train_batch_1, train_batch_2, train_batch_3)
                    total_loss += loss
                    current_step = tf.train.global_step(sess, global_step)
                avg_loss = total_loss / FLAGS.batch_size
                logging.debug(" Epoch {}: avg_loss={}".format(epoch, avg_loss))
                logging.debug(" Evaluation:")
                dev_map = compute_score(sess, model, batched_dev, que_pos_dev)
                logging.debug(" Dev set score: {} (Best={})".format(dev_map, dev_max_map))

                if dev_map > dev_max_map:
                    dev_max_map = dev_map
                    iteration_no_change = 0
                    path = model.saver.save(sess, checkpoint_prefix, global_step=current_step)
                    logging.debug(" Saved model checkpoint to %s" % path)
                else:
                    iteration_no_change += 1
                    logging.debug(" iteration_no_change = {}".format(iteration_no_change))
                    if iteration_no_change >= FLAGS.stopping_criterion:
                        break
            except Exception as exp:
                logging.error(" Exception caught in training: %s " % exp)


# Testing
# ==============================================================
def test():
    with tf.Session() as sess:
        model = create_model(sess)
        accuracy = compute_score(sess, model, batched_test, que_pos_test)
        logging.debug(" precision = {}".format(accuracy))


def calculate_trainable_parameters():
    """
    Return the total number of parameters in the defined/loaded model.
    """
    total_parameters = 0
    for variable in tf.trainable_variables():
        shape = variable.get_shape()
        variable_parameters = 1
        for dim in shape:
            variable_parameters *= dim.value
        total_parameters += variable_parameters
    return total_parameters


def iterative_test():
    stop_messages = ["exit", "stop"]
    with tf.Session() as sess:
        model = create_model(sess)
        sentence = "Tom has a car."
        question = "Who has a car?"
        while sentence not in stop_messages and question not in stop_messages:
            print(sentence)
            print(question)
            question_encoded = data_utils.encode_sen(vocab, question, FLAGS.sequence_size)
            sentence_encoded = data_utils.encode_sen(vocab, sentence, FLAGS.sequence_size)
            x_data_1 = []
            x_data_2 = []
            for i in range(0, FLAGS.batch_size):
                x_data_1.append(question_encoded)
                x_data_2.append(sentence_encoded)

            feed_dict = {
                model.input_x_1: np.array(x_data_1),
                model.input_x_2: np.array(x_data_2),
                model.input_x_3: np.array(x_data_2),
                model.dropout_keep_prob: 1.0    # in testing, dropout probability = 1.0
            }

            batch_scores, accuracy = sess.run([model.cos_12, model.accuracy], feed_dict)
            print("score = {}".format(batch_scores[0]))
            print("accuracy = {}".format(accuracy))
            print("")

            # Read data again ...
            sys.stdout.write("> ")
            sys.stdout.flush()
            sentence = sys.stdin.readline().strip()
            sys.stdout.write("> ")
            sys.stdout.flush()
            question = sys.stdin.readline().strip()


def main(_):
    if FLAGS.test:
        test()
    elif FLAGS.iterative_test:
        iterative_test()
    else:
        train()

if __name__ == "__main__":
    tf.app.run()