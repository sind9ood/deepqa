
import tensorflow as tf

class QaCnnClassifierSimple(object):
    def __init__(self, sequence_length, vocab_size, embedding_size, batch_size, num_classes,
                 filter_sizes, num_filters, param_stddev, l2_reg_lambda):
        """
        Simple cnn classifier based model.
        (1) embeddings are randomly initialized; (2) no overlap features;
        """
        # placeholders for input_q, input_a, output and dropout
        self.input_q = tf.placeholder(tf.int32, [None, sequence_length], name="input_q")
        self.input_a = tf.placeholder(tf.int32, [None, sequence_length], name="input_a")
        self.dropout_in_keep_prob = tf.placeholder(tf.float32, name="dropout_in_keep_prob")
        self.dropout_out_keep_prob = tf.placeholder(tf.float32, name="dropout_out_keep_prob")
        self.input_y = tf.placeholder(tf.float32, [None, num_classes], name="input_y")

        # keep track of l2_loss
        l2_loss = tf.constant(0.0)

        def conv_layer(input, sequence_length, embedding_size, num_filters, filter_sizes):
            """
            Create a general convolution layer.
            """
            pooled_outputs = []
            for i, filter_size in enumerate(filter_sizes):
                with tf.name_scope("conv-maxpool-%s" % filter_size):
                    # convolution layer
                    filter_shape = [filter_size, embedding_size, 1, num_filters]
                    print("qingqing, filter_shape = {}".format(filter_shape))
                    W = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="W")
                    b = tf.Variable(tf.constant(0.1, shape=[num_filters]), name="b")
                    conv = tf.nn.conv2d(input, W, strides=[1, 1, 1, 1], padding="VALID", name="conv")

                    # apply non-linearity
                    # todo: decide the non-linearity function in FLAGS
                    h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")

                    # max-pooling layer
                    pooled = tf.nn.max_pool(h,
                                            ksize=[1, sequence_length-filter_size+1, 1, 1],
                                            strides=[1, 1, 1, 1],
                                            padding="VALID",
                                            name="pool")
                    pooled_outputs.append(pooled)
            # combine all pooled features
            h_pool = tf.concat(3, pooled_outputs)
            return h_pool

        # embedding layer
        with tf.name_scope("embedding"):
            vocab_W = tf.Variable(tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0), name="vocab_W")
            self.vocab_embedded_q = tf.nn.embedding_lookup(vocab_W, self.input_q)
            self.vocab_embedded_a = tf.nn.embedding_lookup(vocab_W, self.input_a)
            # lesson: conv2d operation expects a 4-dimensional tensor with dimensions
            # corresponding to batch, width, height and channel. The result of our embedding
            # operation doesn't contain the channel dimension, so we add it manually,
            # leaving us a layer of shape [None, sequence_length, embedding_size, 1].
            # This is why we run expand_dims operation.
            self.vocab_embedded_expanded_q = tf.expand_dims(self.vocab_embedded_q, -1)
            self.vocab_embedded_expanded_a = tf.expand_dims(self.vocab_embedded_a, -1)

            # todo: here we should add overlapping features, but now it is None
            self.embedded_q = self.vocab_embedded_expanded_q
            self.embedded_a = self.vocab_embedded_expanded_a

        # convolution layer + max-pooling layer for each filter size
        num_filters_total = num_filters * len(filter_sizes)
        self.h_pool_q = conv_layer(self.embedded_q, sequence_length, embedding_size, num_filters, filter_sizes)
        self.h_pool_flat_q = tf.reshape(self.h_pool_q, [-1, num_filters_total])
        self.h_pool_a = conv_layer(self.embedded_a, sequence_length, embedding_size, num_filters, filter_sizes)
        self.h_pool_flat_a = tf.reshape(self.h_pool_a, [-1, num_filters_total])

        # add dropout for hidden
        with tf.name_scope("dropout"):
            self.h_drop_q = tf.nn.dropout(self.h_pool_flat_q, self.dropout_in_keep_prob)
            self.h_drop_a = tf.nn.dropout(self.h_pool_flat_a, self.dropout_in_keep_prob)

        # combine question and answer conv outputs
        combined_num_filters_total = 2 * num_filters_total
        combined_q_a = tf.concat(1, [self.h_drop_q, self.h_drop_a])

        # final scores and predictions
        with tf.name_scope("output"):
            W = tf.get_variable("W",
                                shape=[combined_num_filters_total, num_classes],
                                initializer=tf.contrib.layers.xavier_initializer())
            b = tf.Variable(tf.constant(0.1, shape=[num_classes]), name="b")
            l2_loss += tf.nn.l2_loss(W)
            l2_loss += tf.nn.l2_loss(b)
            self.scores = tf.nn.xw_plus_b(combined_q_a, W, b, name="scores")
            self.predictions = tf.argmax(self.scores, 1, name="predictions")

        # calculate mean cross-entropy loss
        with tf.name_scope("loss"):
            losses = tf.nn.softmax_cross_entropy_with_logits(self.scores, self.input_y)
            self.loss = tf.reduce_mean(losses) + l2_loss * l2_reg_lambda

        # accuracy
        with tf.name_scope("accuracy"):
            correct_predictions = tf.equal(self.predictions, tf.argmax(self.input_y, 1))
            self.accuracy = tf.reduce_mean(tf.cast(correct_predictions, "float"), name="accuracy")

