import os
import sys
import time
import logging
import datetime
import operator

import numpy as np
import tensorflow as tf

import data_utils
import data_reader
from cnn_qa import CnnQaModel
from qa_classifier_model import CnnQaClassifierModel

logging.basicConfig(level=logging.DEBUG)

# tf.flags.DEFINE_integer("sequence_size", 200, "Predfined the size of each sequence (default: 200)")
tf.flags.DEFINE_integer("embedding_dim", 50, "Dimensionality of character embedding. ")
tf.flags.DEFINE_integer("overlap_dim", 5, "Dimensionality of overlapping features. ")
tf.flags.DEFINE_integer("filter_size", 5, "Filter size (default = 5)")
tf.flags.DEFINE_integer("n_filters", 100, "Number of filters (default = 100)")
tf.flags.DEFINE_integer("n_classes", 2, "Number of classes (default = 2)")
tf.flags.DEFINE_integer("stopping_criterion", 10, "Predefined stopping criterion in training (default: 10)")
tf.flags.DEFINE_integer("num_epoches", 999, "Training maximum iterations (default = 100)")
tf.flags.DEFINE_float("dropout_in", 0.2, "Dropout probability for input layer (default = 0.2)")
tf.flags.DEFINE_float("dropout_hid", 0.3, "Dropout probability for hidden layer (default = 0.3)")
# tf.flags.DEFINE_float("dropout_keep_prob", 1.0, "Dropout keep probability. ")
tf.flags.DEFINE_float("learning_rate", 0.001, "Learning rate (default = 0.0001)")
tf.flags.DEFINE_float("param_stddev", 0.1, "TODO?")
tf.flags.DEFINE_float("l2", 0, "L2 regularization weight")
tf.flags.DEFINE_string("dataset", "insurancewiki", "dataset name (default is insurancewiki)")
tf.flags.DEFINE_string("output_dir", "runs", "Directory for saving outputs")
tf.flags.DEFINE_string("data_dir", "/Users/qingqingcai/Documents/corpus/deepqa", "Data directory to training, validation and testing files")
tf.flags.DEFINE_string("embedding_name", "emb_aquaint+wiki.txt.gz.ndim=50.bin.npy", "Pretrained word embedding file")
tf.flags.DEFINE_string("embed_path", "senna.txt", "Pretrained word embedding file")
tf.flags.DEFINE_boolean("test", False, "Evaluate on specific data directory")
tf.flags.DEFINE_boolean("iterative_test", False, "Iterative test based on users' inputs")

# Training parameters
tf.flags.DEFINE_integer("batch_size", 100, "Batch Size (default: 64)")
tf.flags.DEFINE_integer("evaluate_every", 10, "Evaluate model on dev set after this many steps (default: 100)")
tf.flags.DEFINE_integer("checkpoint_every", 10, "Save model after this many steps (default: 100)")

# Misc Parameters
tf.flags.DEFINE_boolean("allow_soft_placement", True, "Allow device soft device placement")
tf.flags.DEFINE_boolean("log_device_placement", False, "Log placement of ops on devices")
tf.flags.DEFINE_boolean("shuffle", True, "Allow shuffle in data (default = True)")

FLAGS = tf.flags.FLAGS
FLAGS._parse_flags()
logging.info("\n Parameters:")
for attr, value in sorted(FLAGS.__flags.items()):
    logging.info(" [*] {} = {}".format(attr.upper(), value))
logging.info("")

# Prepare Parameters and Vocabulary
# ==============================================================
data_dir = os.path.join(FLAGS.data_dir, FLAGS.dataset)
w2v_path = os.path.join(os.curdir, FLAGS.embedding_name)
train_file = os.path.join(data_dir, "train.txt")
valid_file = os.path.join(data_dir, "dev.txt")
test_file = os.path.join(data_dir, "test.txt")

output_dir = os.path.join(FLAGS.output_dir, FLAGS.dataset)
checkpoint_dir = os.path.abspath(os.path.join(output_dir, "checkpoints"))
checkpoint_prefix = os.path.join(checkpoint_dir, "model")

logging.info(" [*] Starting loading data")
# vocab, _ = data_utils.prepare_data(data_dir)
# vocab_emb = data_utils.prepare_word_embedding(w2v_path)
vocab, vocab_emb = data_utils.prepare_data(data_dir, FLAGS.embed_path, FLAGS.embedding_dim)
vocab_emb_overlap_que = data_utils.prepare_overlap_embedding(3, FLAGS.overlap_dim, 123)
vocab_emb_overlap_ans = data_utils.prepare_overlap_embedding(3, FLAGS.overlap_dim, 123)

# print("qingqing test ...........")
# print("{}".format(len(vocab)))
# print("{}, {}".format(len(vocab_emb), len(vocab_emb[0])))
# print("{}, {}".format(len(vocab_emb_overlap_que), len(vocab_emb_overlap_que[0])))
# print("{}, {}".format(len(vocab_emb_overlap_ans), len(vocab_emb_overlap_ans[0])))


# Load Data
# ==============================================================
train_corpus, dev_corpus, test_corpus = data_reader.load_data(data_dir, vocab, FLAGS.shuffle)
logging.info(" [*] Loaded {} train instances".format(train_corpus.num_instances))
logging.info(" [*] Loaded {} dev instances".format(dev_corpus.num_instances))
logging.info(" [*] Loaded {} test instances".format(test_corpus.num_instances))


# print("")
# print("")
# print("")
# print("{}\t{}".format(len(train_corpus.que_tokens_list[0]), train_corpus.que_tokens_list[0]))
# print("{}\t{}".format(len(train_corpus.que_tokens_indexed_list[0]), train_corpus.que_tokens_indexed_list[0]))
# print("{}\t{}".format(len(train_corpus.que_overlap_list[0]), train_corpus.que_overlap_list[0]))
# print("{}\t{}".format(len(train_corpus.sen_tokens_list[0]), train_corpus.sen_tokens_list[0]))
# print("{}\t{}".format(len(train_corpus.sen_tokens_indexed_list[0]), train_corpus.sen_tokens_indexed_list[0]))
# print("{}\t{}".format(len(train_corpus.sen_overlap_list[0]), train_corpus.sen_overlap_list[0]))
# # for t in train_corpus.que_tokens_list[0]:
# #     print("{}:\t{}".format(t, vocab[t]))
# #     print(vocab_emb[vocab[t]])
# print("")
# print("")
# print("")
# print("max_que_len_train = {}".format(train_corpus.sen_max_seq_size))
# print("max_que_len_dev = {}".format(dev_corpus.sen_max_seq_size))
# print("max_que_len_test = {}".format(test_corpus.sen_max_seq_size))


# TODO: how to decide the maximum size
que_max_sent_size = max(train_corpus.que_max_seq_size, dev_corpus.que_max_seq_size)
sen_max_sent_size = max(train_corpus.sen_max_seq_size, dev_corpus.sen_max_seq_size)


def create_model(session, q_max_sent_size, a_max_sent_size):
    model = CnnQaClassifierModel(vocab_emb, vocab_emb_overlap_que, vocab_emb_overlap_ans,
                                 FLAGS.batch_size,
                                 q_max_sent_size, a_max_sent_size,
                                 FLAGS.filter_size, FLAGS.n_filters, FLAGS.n_classes,
                                 FLAGS.param_stddev, FLAGS.learning_rate, FLAGS.l2,
                                 FLAGS.dropout_in, FLAGS.dropout_hid, FLAGS.param_stddev)
    ckpt = tf.train.get_checkpoint_state(checkpoint_dir)

    if (FLAGS.test or FLAGS.iterative_test) and ckpt and tf.gfile.Exists(ckpt.model_checkpoint_path):
        logging.info(" [*] Reading model parameters from %s" % ckpt.model_checkpoint_path)
        model.saver.restore(session, ckpt.model_checkpoint_path)
    else:
        logging.info(" [*] Created model from fresh parameters")

    return model


def compute_score(sess, model, corpus):
    batches = corpus.get_batches(corpus.num_instances, False)  # only one batch, size = #_of_instances
    for batch_idx in range(0, len(batches[0])):
        que_indexed_batch = batches[0][batch_idx]
        sen_indexed_batch = batches[1][batch_idx]
        que_overlap_batch = batches[2][batch_idx]
        sen_overlap_batch = batches[3][batch_idx]
        label_batch = batches[4][batch_idx]
        feed_dict = {
            model.x_q: que_indexed_batch,
            model.x_a: sen_indexed_batch,
            model.x_overlap_q: que_overlap_batch,
            model.x_overlap_a: sen_overlap_batch,
            model.y: label_batch
        }
        y_scores = model.sigmoid_result.eval(feed_dict)
        # print(y_scores[1:10])
        # for ti in range(0, len(y_scores)):
        #     s = y_scores[ti]
        #     if s[0] < s[1]:
        #         print("\t{}, {}".format(s, label_batch[ti]))



        # predicted_labels = model.sigmoid_result.eval(feed_dict)
        # for i in range(0, len(label_batch)):
        #     print("{}:\t{}, {}".format(i, label_batch[i], predicted_labels[i]))




# Training
# ==============================================================
def train():

    with tf.Session() as sess:
        # create model
        model = create_model(sess, que_max_sent_size, sen_max_sent_size)

        global_step = tf.Variable(0, name="global_step", trainable=False)
        optimizer = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate,
                                          beta1=0.9, beta2=0.999, epsilon=1e-8)
        grads_and_vars = optimizer.compute_gradients(model.loss)
        train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)


        # define training procedure
        epoch = 0
        timer_train = time.time()
        best_dev_map = -np.inf
        no_best_dev_update = 0
        num_train_batches = train_corpus.num_instances / FLAGS.batch_size

        logging.info(" [*] Start training ...")
        sess.run(tf.initialize_all_variables())

        while epoch < FLAGS.num_epoches:
            timer_epoch = time.time()
            total_cost = 0
            count = 0
            i = 0
            batches = train_corpus.get_batches(FLAGS.batch_size, True)
            num_batch_iterations = len(batches[0])
            for batch_idx in range(0, num_batch_iterations):
                que_indexed = batches[0][batch_idx]
                sen_indexed = batches[1][batch_idx]
                que_overlap = batches[2][batch_idx]
                sen_overlap = batches[3][batch_idx]
                label = batches[4][batch_idx]

                _, cost, argmax_result, sigmoid_result = sess.run([train_op, model.loss, model.argmax_result, model.sigmoid_result], feed_dict={
                    model.x_q: que_indexed,
                    model.x_a: sen_indexed,
                    model.x_overlap_q: que_overlap,
                    model.x_overlap_a: sen_overlap,
                    model.y: label
                })
                total_cost += cost
                for ti in range(0, len(sigmoid_result)):
                    s = sigmoid_result[ti]
                    if s[0] < s[1]:
                        print("{}\t{}".format(s, label[ti]))

            average_batch_cost = total_cost * 1.0 / num_batch_iterations * 1.0
            logging.info("Epoch {}".format(epoch))
            logging.info("Epoch time:\t{} seconds".format(time.time() - timer_epoch))
            logging.info("Epoch averlage cost:\t{}\n".format(average_batch_cost))
            compute_score(sess, model, dev_corpus)
            epoch += 1

            # # TODO
            if no_best_dev_update >= 10:
                logging.info("Quitting after no updates of the best score on the test set.")
                break

        logging.info("Training time: {:.4f} seconds".format(time.time() - timer_train))

# Testing
# ==============================================================
def test():
    return


def calculate_trainable_parameters():
    """
    Return the total number of parameters in the defined/loaded model.
    """
    total_parameters = 0
    for variable in tf.trainable_variables():
        shape = variable.get_shape()
        variable_parameters = 1
        for dim in shape:
            variable_parameters *= dim.value
        total_parameters += variable_parameters
    return total_parameters


def iterative_test():
    return


def main(_):
    if FLAGS.test:
        test()
    elif FLAGS.iterative_test:
        iterative_test()
    else:
        train()

if __name__ == "__main__":
    tf.app.run()