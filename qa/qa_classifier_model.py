import tensorflow as tf
import qa_utils

class CnnQaClassifierModel(object):
    # todo: Is n_kernels explained as "number of filters"?
    def __init__(self, vocab_emb, vocab_emb_overlap_que, vocab_emb_overlap_ans, batch_size, q_max_sent_size, a_max_sent_size,
                 filter_size, num_filters, n_classes, param_stddev, learning_rate, l2_reg_lambda,
                 dropout_in, dropout_hid, stddev):
        # Input Layer
        self.x_q = tf.placeholder(tf.int32, shape=[None, q_max_sent_size])
        self.x_a = tf.placeholder(tf.int32, shape=[None, a_max_sent_size])
        self.x_overlap_q = tf.placeholder(tf.int32, shape=[None, q_max_sent_size])
        self.x_overlap_a = tf.placeholder(tf.int32, shape=[None, a_max_sent_size])
        self.y = tf.placeholder(tf.float32, shape=[None, n_classes])
        self.batch_size = tf.placeholder(dtype=tf.int32)
        self.filter_size = filter_size
        self.padding = [[0, 0], [self.filter_size-1, self.filter_size-1], [0,0]]
        self.num_filters = num_filters
        self.n_classes = n_classes
        self.learning_rate = learning_rate

        # # blog: http://lan2720.github.io/2016/06/01/tensorflow%E4%B9%8B%E8%B7%AF-%E8%A7%A3%E8%AF%BBtextCNN/

        # define convolutional layer
        def conv_layer(input, filter_shape, n_kernels, conv_space, stddev):
            w_conv = tf.get_variable("conv_w", filter_shape, initializer=tf.random_uniform_initializer(minval=-0.1, maxval=0.1))
            b_conv = tf.get_variable("conv_b", n_kernels, initializer=tf.zeros_initializer)
            h_conv = tf.nn.tanh(tf.nn.conv2d(input, filter=w_conv, strides=[1, 1, self.ndim, 1], padding="SAME") + b_conv)
            h_pool = tf.nn.max_pool(h_conv, ksize=[1, conv_space, 1, 1], strides=[1, conv_space, 1, 1], padding="SAME")
            return tf.reshape(h_pool, [-1, n_kernels])




        with tf.name_scope("embedding_layer"):
            self.vocab_W = tf.Variable(vocab_emb, trainable=False, name="vocab_embeddings", dtype=tf.float32)
            self.overlap_que_W = tf.Variable(vocab_emb_overlap_que, trainable=True, name="overlap_que_embeddings", dtype=tf.float32)
            self.overlap_ans_W = tf.Variable(vocab_emb_overlap_ans, trainable=True, name="overlap_ans_embeddings", dtype=tf.float32)

            self.q_emb = tf.cast(qa_utils.get_embeddings(self.x_q, self.vocab_W, self.padding), dtype=tf.float32)
            # embed_scope.reuse_variables()
            self.a_emb = tf.cast(qa_utils.get_embeddings(self.x_a, self.vocab_W, self.padding), dtype=tf.float32)

            self.q_overlap_emb = tf.cast(qa_utils.get_embeddings(self.x_overlap_q, self.overlap_que_W, self.padding), dtype=tf.float32)
            # overlap_embed_scope.reuse_variables()
            self.a_overlap_emb = tf.cast(qa_utils.get_embeddings(self.x_overlap_a, self.overlap_ans_W, self.padding), dtype=tf.float32)

            self.q_emb_concat = tf.concat(2, [self.q_emb, self.q_overlap_emb])
            self.a_emb_concat = tf.concat(2, [self.a_emb, self.a_overlap_emb])

        if dropout_in > 0:
            self.q_emb_concat = tf.nn.dropout(self.q_emb_concat, dropout_in)
            self.a_emb_concat = tf.nn.dropout(self.a_emb_concat, dropout_in)

        ##########################################
        self.ndim = vocab_emb.shape[1] + vocab_emb_overlap_que.shape[1]
        self.conv_space_q = q_max_sent_size
        self.conv_space_a = a_max_sent_size

        filter_shape = [filter_size, self.ndim, 1, self.num_filters]
        self.q_emb_resized = tf.reshape(self.q_emb_concat, (-1, self.conv_space_q, self.ndim, 1))
        self.a_emb_resized = tf.reshape(self.a_emb_concat, (-1, self.conv_space_a, self.ndim, 1))
        with tf.variable_scope("conv_scope") as conv_scope:
            self.h_pool_q = conv_layer(self.q_emb_resized, filter_shape, self.num_filters, self.conv_space_q, param_stddev)
            conv_scope.reuse_variables()
            self.h_pool_a = conv_layer(self.a_emb_resized, filter_shape, self.num_filters, self.conv_space_a, param_stddev)


        with tf.name_scope("hidden_linear"):
            h_hid_w = tf.Variable(tf.random_uniform([self.num_filters, self.num_filters], stddev), name="hidden_weight")
            self.q_hid_out = tf.matmul(self.h_pool_q, h_hid_w)
            # TODO: this is wield, since in java version only compute q_hid_out, not a_hid_out
        #    self.a_hid_out = tf.matmul(self.h_pool_a, h_hid_w)
            self.a_hid_out = self.h_pool_a

        #######################################################
        # Combine results
        M = tf.Variable(tf.random_uniform([num_filters, num_filters], minval=-0.1, maxval=0.1))
        dotProd = tf.matmul(tf.matmul(self.q_hid_out, M), tf.transpose(self.a_hid_out))
        diag = tf.reshape(tf.diag_part(dotProd), [-1, 1])
        concat = tf.concat(1, [self.q_hid_out, diag, self.a_hid_out])

        # Fully connected linear layers (2*n_kernels, 2*n_kernels)
        num_filters_total = 2 * self.num_filters + 1
        # h_w = tf.Variable(tf.random_uniform([num_filters_total, num_filters_total], minval=-0.1, maxval=0.1), name="hidden_w")
        h_w = tf.get_variable("hidden_w",
                              shape=[num_filters_total, num_filters_total],
                              initializer=tf.contrib.layers.xavier_initializer())
        h_b = tf.Variable(tf.constant(1.0, shape=[num_filters_total]), name="hidden_b")
        self.hid_out = tf.nn.tanh(tf.nn.xw_plus_b(concat, h_w, h_b), name="hidden_out")

        if dropout_hid > 0:
            self.hid_out = tf.nn.dropout(self.hid_out, dropout_hid)

        # Output Layer (2*n_kernels, n_classes)
        with tf.name_scope("output"):
            l2_loss = tf.constant(0.0)
            # output_w = tf.Variable(tf.random_uniform([num_filters_total, self.n_classes], minval=-0.1, maxval=0.1), name="output_w")
            output_w = tf.get_variable("output_w",
                                       shape=[num_filters_total, self.n_classes],
                                       initializer=tf.contrib.layers.xavier_initializer())
            output_b = tf.Variable(tf.constant(0.1, shape=[self.n_classes]), name="output_b")
            l2_loss += tf.nn.l2_loss(output_w)
            l2_loss += tf.nn.l2_loss(output_b)
            self.scores = tf.nn.xw_plus_b(self.hid_out, output_w, output_b)
            # self.argmax_result = tf.argmax(self.scores, 1, name="argmax_result")
            # self.sigmoid_result = tf.nn.softmax(self.scores, name="sparse_softmax")
            self.predictions = tf.argmax(self.scores, 1, name="predictions")

        with tf.name_scope("loss"):
            print(self.scores)
            print(self.y)
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(self.scores, self.y)
            self.loss = tf.reduce_mean(losses) + l2_reg_lambda * l2_loss

        with tf.name_scope("accuracy"):
        #     correct_predictions = tf.equal(self.predictions, tf.argmax(self.y, 1))
        #     self.accuracy = tf.reudce_mean(tf.cast(correct_predictions, "float"), name="accuracy")
            self.accuracy = 0

        tf.scalar_summary(self.loss.op.name, self.loss)

        # Saver
        self.saver = tf.train.Saver(tf.all_variables())