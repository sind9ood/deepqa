import tensorflow as tf
import numpy as np

class CnnQaRankingModel(object):
    def __init__(self, vocab_emb, embedding_size, max_sequence_length, batch_size,
                 filter_sizes, num_filters, dropout_keep_prob, l2_reg_lambda):
        self.input_x_que = tf.placeholder(tf.int32, [None, max_sequence_length], name="input_que")
        self.input_x_pos = tf.placeholder(tf.int32, [None, max_sequence_length], name="input_pos")
        self.input_x_neg = tf.placeholder(tf.int32, [None, max_sequence_length], name="input_neg")
        l2_loss = tf.constant(0.0)

        # Embedding Layer
        with tf.name_scope("embedding"):
            W = tf.Variable(vocab_emb, name="W")
            self.embedded_chars_que = tf.nn.embedding_lookup(W, self.input_x_que)
            self.embedded_chars_pos = tf.nn.embedding_lookup(W, self.input_x_pos)
            self.embedded_chars_neg = tf.nn.embedding_lookup(W, self.input_x_neg)

        self.embedded_chars_que_expanded = tf.expand_dims(self.embedded_chars_que, -1)
        self.embedded_chars_pos_expanded = tf.expand_dims(self.embedded_chars_pos, -1)
        self.embedded_chars_neg_expanded = tf.expand_dims(self.embedded_chars_neg, -1)

        def conv_layer(input, filter_shape, filter_size, num_filters, sequence_length, name):
            W = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="W")
            b = tf.Variable(tf.constant(0.1, shape=[num_filters]), name="b")
            conv = tf.nn.conv2d(
                input,
                W,
                strides=[1, 1, 1, 1],
                padding='VALID',
                name='conv-%s' % name
            )
            h = tf.nn.relu(tf.nn.bias_add(conv, b), name='relu-%s' % name)
            pooled = tf.nn.max_pool(
                h,
                ksize=[1, sequence_length - filter_size + 1, 1, 1],
                strides=[1, 1, 1, 1],
                padding='VALID',
                name='pool-%s' % name
            )
            return pooled


        pooled_outputs_que = []
        pooled_outputs_pos = []
        pooled_outputs_neg = []
        for i, filter_size in enumerate(filter_sizes):
            with tf.name_scope("conv-maxpool-%s" % filter_size):
                filter_shape = [filter_size, embedding_size, 1, num_filters]
                pooled_outputs_que.append(conv_layer(
                    self.embedded_chars_que_expanded,
                    filter_shape,
                    num_filters,
                    max_sequence_length,
                    "que"
                ))

                pooled_outputs_pos.append(conv_layer(
                    self.embedded_chars_pos_expanded,
                    filter_shape,
                    filter_size,
                    num_filters,
                    max_sequence_length,
                    "pos"
                ))

                pooled_outputs_neg.append(conv_layer(
                    self.embedded_chars_neg_expanded,
                    filter_shape,
                    filter_size,
                    num_filters,
                    max_sequence_length,
                    "neg"
                ))

        num_filters_total = num_filters * len(filter_sizes)
        pooled_reshape_que = tf.reshape(tf.concat(3, pooled_outputs_que), [-1, num_filters_total])
        pooled_reshape_pos = tf.reshape(tf.concat(3, pooled_outputs_pos), [-1, num_filters_total])
        pooled_reshape_neg = tf.reshape(tf.concat(3, pooled_outputs_neg), [-1, num_filters_total])

        pooled_flat_que = tf.nn.dropout(pooled_reshape_que, dropout_keep_prob)
        pooled_flat_pos = tf.nn.dropout(pooled_reshape_pos, dropout_keep_prob)
        pooled_flat_neg = tf.nn.dropout(pooled_reshape_neg, dropout_keep_prob)

        pooled_len_que = tf.sqrt(tf.reduce_sum(tf.mul(pooled_flat_que, pooled_flat_que), 1))
        pooled_len_pos = tf.sqrt(tf.reduce_sum(tf.mul(pooled_flat_pos, pooled_flat_pos), 1))
        pooled_len_neg = tf.sqrt(tf.reduce_sum(tf.mul(pooled_flat_neg, pooled_flat_neg), 1))

        pooled_mul_que_pos = tf.reduce_sum(tf.mul(pooled_flat_que, pooled_flat_pos), 1)
        pooled_mul_que_neg = tf.reduce_sum(tf.mul(pooled_flat_que, pooled_flat_neg), 1)

        with tf.name_scope("output"):
            self.cos_que_pos = tf.div(pooled_mul_que_pos, tf.mul(pooled_len_que, pooled_len_pos))
            self.cos_que_neg = tf.div(pooled_mul_que_neg, tf.mul(pooled_len_que, pooled_len_neg))

        zero = tf.constant(0, dtype=tf.float32)
        margin = tf.constant(0.05, dtype=tf.float32)

        with tf.name_scope("loss"):
            self.losses = tf.maximum(zero, tf.sub(margin, tf.sub(self.cos_que_pos, self.cos_que_neg)))
            self.loss = tf.reduce_sum(self.losses) + l2_reg_lambda * l2_loss

        with tf.name_scope("accuracy"):
            self.correct = tf.equal(zero, self.losses)
            self.accuracy = tf.reduce_mean(tf.cast(self.correct, "float"), name="accuracy")

