import numpy
import os
import tensorflow as tf
import time

from eval import map_score, precision_score, get_map_score
from reader import getSickDataSet

import warnings
warnings.filterwarnings("ignore")  # TODO remove

def KLDivergenceDist(pred, target, len):
    packed = tf.pack([pred, target])
    tr_packed = tf.transpose(packed, perm=[1, 0, 2])

    def get_score(pair):
        pred_t, target_t = tf.unpack(pair)
        return tf.select(tf.logical_or(tf.equal(pred_t, 0.0), tf.equal(target_t, 0.0)), tf.constant(0.0, shape=[len]),
                            tf.mul(target_t, tf.sub(tf.log(target_t), tf.log(pred_t))))

    return tf.reduce_sum(tf.map_fn(get_score, tr_packed), [0, 1])


def main():
    data_dir = 'sick'
    # problemType = 'INF'
    problemType = 'SIM'

    # Configuration parameters
    n_classes = 5
    n_epochs = 100
    batch_size = 25
    learning_rate = 0.0001
    max_norm = 0
    dropout_rate = 0.2
    filterWidth = 5
    nkernels = 100
    param_stddev = 1.0


    trainData, devData, testData, vocab_emb = getSickDataSet(data_dir, type=problemType)

    q_max_sent_size = trainData.dataset[0].shape[1]
    a_max_sent_size = trainData.dataset[1].shape[1]
    ndim = vocab_emb.shape[1]

    print 'batch_size', batch_size
    print 'n_epochs', n_epochs
    print 'learning_rate', learning_rate
    print 'max_norm', max_norm

    # Create input variables
    x_q = tf.placeholder(tf.int32, shape=[None, q_max_sent_size])
    x_a = tf.placeholder(tf.int32, shape=[None, a_max_sent_size])
    if problemType == 'SIM':
        y_ = tf.placeholder(tf.float32, shape=[None, 5])
        label = tf.placeholder(tf.float32, shape=[None, 1])
    else:
        y_ = tf.placeholder(tf.int64, shape=[None])

    def get_embeddings(input, embedding, paddings):
        # embedding = tf.get_variable("embedding", vocab_emb.shape, trainable=False)
        return tf.pad(tf.nn.embedding_lookup(embedding, input), paddings, "CONSTANT")

    paddings = [[0,0], [filterWidth-1, filterWidth-1], [0,0]]
    with tf.variable_scope("embedding_scope") as emb_scope:
        embedding = tf.get_variable("embedding", vocab_emb.shape, trainable=False)
        q_emb = tf.cast(get_embeddings(x_q, embedding, paddings), dtype=tf.float32)
        emb_scope.reuse_variables()
        a_emb = tf.cast(get_embeddings(x_a, embedding, paddings), dtype=tf.float32)

    conv_space_Q = q_max_sent_size + 2*(filterWidth-1)
    conv_space_A = a_max_sent_size + 2*(filterWidth-1)

    # Define convolutional layer
    def conv_layer(input, filter_shape, nkernels, conv_space):
        W_conv = tf.get_variable("conv_W", filter_shape, initializer=tf.random_uniform_initializer(minval=-0.1, maxval=0.1), trainable=True)
        b_conv = tf.get_variable("conv_b", nkernels, initializer=tf.zeros_initializer, trainable=True)
        # h_conv = tf.nn.tanh(tf.nn.conv2d(input, filter=W_conv, strides=[1, 1, ndim, 1], padding="SAME") + b_conv)
        h_conv = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d(input, filter=W_conv, strides=[1, 1, ndim, 1], padding="SAME"), b_conv))
        h_pool = tf.nn.max_pool(h_conv, ksize=[1, conv_space, 1, 1], strides=[1, conv_space, 1, 1], padding='SAME')
        return tf.reshape(h_pool, [-1, nkernels])

    filter_shape = [filterWidth, ndim, 1, nkernels] # [height, width, in_channels, out_channels]
    q_emb_resized = tf.reshape(q_emb, (-1, conv_space_Q, ndim, 1))
    a_emb_resized = tf.reshape(a_emb, (-1, conv_space_A, ndim, 1))
    with tf.variable_scope("conv_scope") as conv_scope:
        h_pool_q = conv_layer(q_emb_resized, filter_shape, nkernels, conv_space_Q)
        conv_scope.reuse_variables()
        h_pool_a = conv_layer(a_emb_resized, filter_shape, nkernels, conv_space_A)

    # Combine result
    sub = tf.abs(tf.sub(h_pool_q, h_pool_a))
    mul = tf.mul(h_pool_q, h_pool_a)
    concat = tf.concat(1, [sub, mul])

    # Fully connected linear layer (2*nkernels, 2*nkernels)
    h_w = tf.Variable(tf.random_uniform([2*nkernels, 2*nkernels], minval=-0.2, maxval=0.2), name="hidden_w", trainable=True)
    h_b = tf.Variable(tf.zeros([2*nkernels]), name="hidden_b", trainable=True)
    hid_out = tf.nn.tanh(tf.nn.xw_plus_b(concat, h_w, h_b), "hidden_out")

    # Output layer (2*nkernels, nclasses)
    output_w = tf.Variable(tf.random_uniform([2*nkernels, n_classes], minval=-0.2, maxval=0.2), name="output_w", trainable=True)
    output_b = tf.Variable(tf.zeros([n_classes]), name="output_b", trainable=True)
    y_conv = tf.nn.sigmoid(tf.nn.xw_plus_b(hid_out, output_w, output_b))

    if problemType == 'INF':
        loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(y_conv, y_))
        prediction = tf.arg_max(y_conv, 1)
        correct_prediction = tf.equal(y_, prediction)
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    elif problemType == 'SIM':
        y_norm = tf.nn.softmax(y_conv)
        loss = KLDivergenceDist(y_norm, y_, 5)
        # loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, y_))
        sim = tf.matmul(y_norm, tf.reshape(tf.constant([1.0, 2.0, 3.0, 4.0, 5.0]), (-1, 1)))
        sim_sub = tf.sub(sim, tf.reduce_mean(sim))
        label_sub = tf.sub(label, tf.reduce_mean(label))
        pearson = tf.reshape(tf.matmul(tf.transpose(sim_sub), label_sub) / (tf.sqrt(tf.nn.l2_loss(sim_sub)*2) * tf.sqrt(tf.nn.l2_loss(label_sub)*2)), [1])

    tf.scalar_summary(loss.op.name, loss)
    global_step = tf.Variable(0, name='global_step', trainable=False)

    tvars = tf.trainable_variables()
    grads, _ = tf.clip_by_global_norm(tf.gradients(loss, tvars), 5)
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train_op = optimizer.apply_gradients(zip(grads, tvars), global_step=global_step)

    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())
        sess.run(tf.assign(embedding, vocab_emb))
        # sess.run(tf.assign(overlap_embedding, vocab_emb_overlap))

        best_dev_acc = -numpy.inf
        best_test_acc = -numpy.inf

        epoch = 0
        timer_train = time.time()
        no_best_dev_update = 0
        num_train_batches = trainData.size / batch_size

        saver = tf.train.Saver()

        while epoch < n_epochs:
            timer = time.time()
            i = 0
            tot_cost = 0
            count = 0
            while i < num_train_batches:
                batch = trainData.getNext(batch_size)
                feed_dict = {x_q: batch[0], x_a: batch[1], y_: batch[4]}
                _, cost = sess.run([train_op, loss], feed_dict=feed_dict)
                tot_cost += cost
                count += 1

                if (i+1) % 20 == 0 or (i+1) == num_train_batches:
                    if problemType == "SIM":
                        feed_dict={x_q: devData.dataset[0], x_a: devData.dataset[1], label: numpy.reshape(devData.dataset[4], [-1, 1])}
                        dev_acc = sess.run(pearson[0], feed_dict=feed_dict)
                    else:
                        feed_dict={x_q: devData.dataset[0], x_a: devData.dataset[1], y_: devData.dataset[4]}
                        dev_acc = sess.run(accuracy, feed_dict=feed_dict)
                    print('epoch: {} batch: {} train cost: {:.6f}; accuracy: {:.4f}'.format(epoch, i, tot_cost/count, dev_acc))
                    tot_cost = 0
                    count = 0
                    # get_map_score(qids_dev, devData.dataset[4], y_pred_dev, './eval')

                    if dev_acc > best_dev_acc:
                        if problemType == "SIM":
                            feed_dict={x_q: testData.dataset[0], x_a: testData.dataset[1], label: numpy.reshape(testData.dataset[4], [-1, 1])}
                            test_acc = sess.run(pearson[0], feed_dict=feed_dict)
                        else:
                            feed_dict={x_q: testData.dataset[0], x_a: testData.dataset[1], y_: testData.dataset[4]}
                            test_acc = sess.run(accuracy, feed_dict=feed_dict)

                        print('epoch: {} batch: {} dev acc: {:.4f}; test acc: {:.4f};'.format(epoch, i, dev_acc, test_acc))

                        best_dev_acc = dev_acc
                        save_path = saver.save(sess, "model.ckpt", global_step=global_step)
                        print("Model saved in file: %s" % save_path)
                        no_best_dev_update = 0
                i += 1

            if no_best_dev_update >= 10:
                print "Quitting after of no update of the best score on test set", no_best_dev_update
                break

            print('epoch {} took {:.4f} seconds'.format(epoch, time.time() - timer))
            epoch += 1
            no_best_dev_update += 1

        print('Training took: {:.4f} seconds'.format(time.time() - timer_train))
        # saver.restore(sess, "model.ckpt")
        # test_acc = map_score(qids_test, y_test, y_pred_test) * 100
        # fname = os.path.join('./out', 'best_dev_params.epoch={:02d};batch={:05d};dev_acc={:.2f}.dat'.format(epoch, i, best_dev_acc))
        # numpy.savetxt(os.path.join('./out', 'test.epoch={:02d};batch={:05d};dev_acc={:.2f}.predictions.npy'.format(epoch, i, best_dev_acc)), y_pred_test)

if __name__ == '__main__':
    main()
