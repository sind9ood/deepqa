# Copyright 2015 Google Inc. All Rights Reserved.
"""Utilities for parsing QA text files."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import math

import numpy as np
import tensorflow as tf


class Vocab:
    __slots__ = ["word2index", "index2word", "unknown"]

    def __init__(self, index2word=None, maxFreq=None):
        self.word2index = {}
        self.index2word = []
        self.maxFreqIgnore = maxFreq;

        # add unknown word:
        self.add_words(["**UNKNOWN**"])
        self.unknown = 0

        if index2word is not None:
            self.add_words(index2word)

    def add_words(self, words, word_freq=None):
        if type(words) is str:
            words = [words]

        for word in words:
            if word_freq is None:
                if word not in self.word2index:
                    self.word2index[word] = len(self.word2index)
                    self.index2word.append(word)
            else:
                # Filter words of which frequency are larger than 3
                if word_freq[word] > self.maxFreqIgnore:
                    if word not in self.word2index:
                        self.word2index[word] = len(self.word2index)
                        self.index2word.append(word)

    def __call__(self, line):
        """
        Convert from numerical representation to words
        and vice-versa.
        """
        if type(line) is np.ndarray:
            return " ".join([self.index2word[word] for word in line])
        if type(line) is list:
            if len(line) > 0:
                if line[0] is int:
                    return " ".join([self.index2word[word] for word in line])
            indices = np.zeros(len(line), dtype=np.int32)
        else:
            line = line.split(" ")
            indices = np.zeros(len(line), dtype=np.int32)

        for i, word in enumerate(line):
            indices[i] = self.word2index.get(word, self.unknown)

        return indices

    @property
    def size(self):
        return len(self.index2word)

    def __len__(self):
        return len(self.index2word)


class dataSet():
    def __init__(self, dataset):
        self.index = 0
        self.dataset = dataset
        self.len = len(dataset[0])

    def getNext(self, num):
        # index = np.random.randint(0, self.size)
        index = self.index
        self.index += num
        if index + num > self.len:
            margin = num - (self.size - index)
            self.index = margin
            return [np.concatenate((self.dataset[0][index:self.len], self.dataset[0][0:margin]), axis=0),
                    np.concatenate((self.dataset[1][index:self.len], self.dataset[1][0:margin]), axis=0),
                    np.concatenate((self.dataset[2][index:self.len], self.dataset[2][0:margin]), axis=0),
                    np.concatenate((self.dataset[3][index:self.len], self.dataset[3][0:margin]), axis=0),
                    np.concatenate((self.dataset[4][index:self.len], self.dataset[4][0:margin]), axis=0)]
        else:
            return [self.dataset[0][index:index+num], self.dataset[1][index:index+num], self.dataset[2][index:index+num],
                    self.dataset[3][index:index+num], self.dataset[4][index:index+num]]

    @property
    def size(self): return self.len



def _read_words(filename):
    with tf.gfile.GFile(filename, "r") as f:
        # return f.read().replace("\n", "<eos>").split()
        return f.read().split();


# def _build_vocab(filename):
#   data = _read_words(filename)
#
#   counter = collections.Counter(data)
#   count_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))
#
#   words, _ = list(zip(*count_pairs))
#   word_to_id = dict(zip(words, range(len(words))))
#
#   return word_to_id


def _build_vocab_from_WV(Word2Vec):
    """
    Build vocab from words in word vectors
    """
    vocab = Vocab()
    for key in Word2Vec.keys():
        vocab.add_words(key)
    return vocab


def _read_WV(filename):
    with tf.gfile.GFile(filename, "r") as f:
        wv_lines = f.read().split('\n')

    WV = {}
    for line in wv_lines:
        splitted = line.split()
        if len(splitted) > 0:
            WV[splitted[0]] = splitted[1:]
        else:
            print(line)
    return WV


# def _file_to_word_ids(filename, word_to_id):
#   data = _read_words(filename)
#   return [word_to_id[word] for word in data]


def read_raw_data(data_path=None, type='SIM', train=True):
    """
    Load raw data from data directory "data_path".
    """
    x_1 = []
    x_2 = []
    y_ = []

    file = open(os.path.join(data_path))
    for i, line in enumerate(file):
        if i != 0:
            contents = line.split('\t')
            x_1.append(contents[1])
            x_2.append(contents[2])

            if type == 'SIM':
                # 5 Classes (Sentence similarity)
                if train:
                    targets = np.zeros(5)
                    sim = float(contents[3]) - 1
                    ceil = math.ceil(sim)
                    floor = math.floor(sim)
                    if ceil == floor:
                        targets[int(floor)] = 1
                    else:
                        targets[int(floor)] = ceil - sim
                        targets[int(ceil)] = sim - floor
                    y_.append(targets)
                else:
                    y_.append(float(contents[3]))
            else:
                # 3 Classes (Inference)
                if contents[4].strip('\n') == "CONTRADICTION":
                    y_.append(0)
                elif contents[4].strip('\n') == "NEUTRAL":
                    y_.append(1)
                else: y_.append(2)

    return x_1, x_2, y_


def get_index_list(data, vocab, max_length):
    indices = np.zeros([len(data), max_length], dtype=np.int32)
    for i, sent in enumerate(data):
        for j, id in enumerate(vocab(sent.lower())):
            indices[i][j] = int(id)
    return indices


# def something():
#     word_to_id = _build_vocab(train_path)
#     train_data = _file_to_word_ids(train_path, word_to_id)
#     valid_data = _file_to_word_ids(valid_path, word_to_id)
#     test_data = _file_to_word_ids(test_path, word_to_id)
#     vocabulary = len(word_to_id)
#     return train_data, valid_data, test_data, vocabulary


# def data_iterator(raw_data, batch_size, num_steps):
#     """Iterate on the raw PTB data.
#
#     This generates batch_size pointers into the raw PTB data, and allows
#     minibatch iteration along these pointers.
#
#     Args:
#       raw_data: one of the raw data outputs from ptb_raw_data.
#       batch_size: int, the batch size.
#       num_steps: int, the number of unrolls.
#
#     Yields:
#       Pairs of the batched data, each a matrix of shape [batch_size, num_steps].
#       The second element of the tuple is the same data time-shifted to the
#       right by one.
#
#     Raises:
#       ValueError: if batch_size or num_steps are too high.
#     """
#     raw_data = np.array(raw_data, dtype=np.int32)
#
#     data_len = len(raw_data)
#     batch_len = data_len // batch_size
#     data = np.zeros([batch_size, batch_len], dtype=np.int32)
#     for i in range(batch_size):
#         data[i] = raw_data[batch_len * i:batch_len * (i + 1)]
#
#     epoch_size = (batch_len - 1) // num_steps
#
#     if epoch_size == 0:
#         raise ValueError("epoch_size == 0, decrease batch_size or num_steps")
#
#     for i in range(epoch_size):
#         x = data[:, i * num_steps:(i + 1) * num_steps]
#         y = data[:, i * num_steps + 1:(i + 1) * num_steps + 1]
#         yield (x, y)


def build_vocabEmbedding(vocab, embed):
    wv_dim = len(embed['test'])
    vocab_emb = np.zeros((vocab.size, wv_dim), dtype=np.float32)
    for index, word in enumerate(vocab.index2word):
        if word != "**UNKNOWN**":
            vocab_emb[index] = np.array(embed[word])
    return vocab_emb


def get_sequence_lengths(all_sets):
    max_length_1 = 0
    max_length_2 = 0
    all_seq_lengths = []

    for data in all_sets:
        seq_lengths_1 = []
        seq_lengths_2 = []

        # Sent 1
        for sent in data[0]:
            len_sent = len(sent.split())
            seq_lengths_1.append(len_sent)
            if len_sent > max_length_1:
                max_length_1 = len_sent
        # Sent 2
        for sent in data[1]:
            len_sent = len(sent.split())
            seq_lengths_2.append(len_sent)
            if len_sent > max_length_2:
                max_length_2 = len_sent

        all_seq_lengths.append([np.array(seq_lengths_1, dtype=np.int32), np.array(seq_lengths_2, dtype=np.int32)])

    return all_seq_lengths[0], all_seq_lengths[1], all_seq_lengths[2], max_length_1, max_length_2


def getSickDataSet(data_dir, type="SIM"):
    WV = _read_WV("senna.txt")
    vocab = _build_vocab_from_WV(WV)

    train_data = read_raw_data(os.path.join(data_dir+'/train.txt'), type)
    dev_data = read_raw_data(os.path.join(data_dir+'/trial.txt'), type, train=False)
    test_data = read_raw_data(os.path.join(data_dir+'/test.txt'), type, train=False)

    train_seq_lengths, dev_seq_lengths, test_seq_lengths, max_q, max_a = get_sequence_lengths([train_data, dev_data, test_data])

    train_inst_x1 = get_index_list(train_data[0], vocab, max_q)
    train_inst_x2 = get_index_list(train_data[1], vocab, max_a)

    dev_inst_x1 = get_index_list(dev_data[0], vocab, max_q)
    dev_inst_x2 = get_index_list(dev_data[1], vocab, max_a)

    test_inst_x1 = get_index_list(test_data[0], vocab, max_q)
    test_inst_x2 = get_index_list(test_data[1], vocab, max_a)

    trainSet = dataSet([train_inst_x1, train_inst_x2, train_seq_lengths[0], train_seq_lengths[1], np.array(train_data[2])])
    devSet = dataSet([dev_inst_x1, dev_inst_x2, dev_seq_lengths[0], dev_seq_lengths[1], np.array(dev_data[2])])
    testSet = dataSet([test_inst_x1, test_inst_x2, test_seq_lengths[0], test_seq_lengths[1], np.array(test_data[2])])

    vocab_emb = build_vocabEmbedding(vocab, WV)

    return trainSet, testSet, devSet, vocab_emb

if __name__ == '__main__':
    getSickDataSet('sick')