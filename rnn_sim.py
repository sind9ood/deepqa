from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import tensorflow as tf
import numpy

from reader import getSickDataSet

flags = tf.flags
logging = tf.logging

flags.DEFINE_string(
    "model", "small",
    "A type of model. Possible options are: small, medium, large.")
flags.DEFINE_string("data_path", None, "data_path")

FLAGS = flags.FLAGS


class myConfig(object):
    """Tiny config, for testing."""
    problemType = 'INF'
    num_classes = 3
    # mean = 0.0
    # stddev = 0.5
    init_scale = 0.04
    learning_rate = 0.001
    max_grad_norm = 3
    num_layers = 1
    hidden_size = 100
    max_epoch = 1
    max_max_epoch = 100
    keep_prob = 1.0
    lr_decay = 0.9
    batch_size = 24
    vocab_size = 100000
    embed_dim = 50


class QAModel(object):
    def __init__(self, is_training, config):
        self.num_step_q = config.num_steps_q
        self.num_step_a = config.num_steps_a
        size = config.hidden_size

        self.x_q = tf.placeholder(tf.int32, shape=[None, self.num_step_q])
        self.x_a = tf.placeholder(tf.int32, shape=[None, self.num_step_a])
        self.x_q_length = tf.placeholder(tf.int32, shape=[None])
        self.x_a_length = tf.placeholder(tf.int32, shape=[None])

        if config.problemType == 'SIM':
            self.y_ = tf.placeholder(tf.float32, shape=[None, config.num_classes])
            self.label = tf.placeholder(tf.float32, shape=[None, 1])
        else:
            self.y_ = tf.placeholder(tf.int64, shape=[None])

        with tf.device("/cpu:0"):
            with tf.variable_scope("embedding_scope") as emb_scope:
                self.embedding = tf.get_variable("embedding", [config.vocab_size, config.embed_dim], trainable=False)
                q_emb = tf.cast(tf.nn.embedding_lookup(self.embedding, self.x_q), dtype=tf.float32)
                emb_scope.reuse_variables()
                a_emb = tf.cast(tf.nn.embedding_lookup(self.embedding, self.x_a), dtype=tf.float32)

        q_inputs = [tf.reshape(i, (-1, config.embed_dim)) for i in tf.split(1, self.num_step_q, q_emb)]
        a_inputs = [tf.reshape(i, (-1, config.embed_dim)) for i in tf.split(1, self.num_step_a, a_emb)]

        with tf.variable_scope("RNN") as rnn_scope:
            cell = tf.nn.rnn_cell.GRUCell(size)
            if is_training and config.keep_prob < 1:
                cell = tf.nn.rnn_cell.DropoutWrapper(
                    cell, output_keep_prob=config.keep_prob)

            cell_outputs_q, state_q = tf.nn.rnn(cell, q_inputs, dtype=tf.float32, sequence_length=self.x_q_length, scope=rnn_scope)
            rnn_scope.reuse_variables()
            cell_outputs_a, state_a = tf.nn.rnn(cell, a_inputs, dtype=tf.float32, sequence_length=self.x_a_length, scope=rnn_scope)

        output_q = tf.div(tf.add_n(cell_outputs_q), tf.to_float(tf.expand_dims(self.x_q_length, -1)))
        output_a = tf.div(tf.add_n(cell_outputs_a), tf.to_float(tf.expand_dims(self.x_a_length, -1)))

        # Combine result
        sub = tf.abs(tf.sub(output_q, output_a))
        mul = tf.mul(output_q, output_a)
        concat = tf.concat(1, [sub, mul])

        # Fully connected linear layer
        h_w = tf.get_variable("hidden_w", [2*size, 2*size])
        h_b = tf.get_variable("hidden_b", [2*size])
        hid_out = tf.nn.tanh(tf.nn.xw_plus_b(concat, h_w, h_b))

        # Output layer
        output_w = tf.get_variable("output_w", [2*size, config.num_classes])
        output_b = tf.get_variable("output_b", [config.num_classes])
        y_logits = tf.nn.sigmoid(tf.nn.xw_plus_b(hid_out, output_w, output_b))

        if config.problemType == 'INF':
            self._cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(y_logits, self.y_))
            prediction = tf.arg_max(y_logits, 1)
            correct_prediction = tf.equal(self.y_, prediction)
            self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        elif config.problemType == 'SIM':
            self._cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_logits, self.y_))
            sim = tf.matmul(y_logits, tf.reshape(tf.constant([1.0, 2.0, 3.0, 4.0, 5.0]), (-1, 1)))
            sim_sub = tf.sub(sim, tf.reduce_mean(sim))
            label_sub = tf.sub(self.label, tf.reduce_mean(self.label))
            self.pearson = tf.reshape(tf.matmul(tf.transpose(sim_sub), label_sub) / (tf.sqrt(tf.nn.l2_loss(sim_sub)*2) * tf.sqrt(tf.nn.l2_loss(label_sub)*2)), [1])

        if not is_training:
            return

        self._lr = tf.Variable(0.0, trainable=False)
        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(self._cost, tvars),
                                          config.max_grad_norm)
        optimizer = tf.train.AdamOptimizer(self.lr)
        self._train_op = optimizer.apply_gradients(zip(grads, tvars))

    def assign_lr(self, session, lr_value):
        session.run(tf.assign(self.lr, lr_value))

    def assign_embedding(self, session, vocab_emb):
        session.run(tf.assign(self.embedding, vocab_emb))

    # @property
    def x_a(self):
        return self.x_a

    # @property
    def x_q(self):
        return self.x_q

    # @property
    def y_(self):
        return self.y_

    @property
    def cost(self):
        return self._cost

    @property
    def final_state(self):
        return self._final_state

    @property
    def lr(self):
        return self._lr

    @property
    def train_op(self):
        return self._train_op

def run_epoch(session, m, data, eval_op, config, verbose=False):
    start_time = time.time()
    costs = 0.0

    num_batches = int(data.size / config.batch_size)
    for step in xrange(num_batches):
        batch = data.getNext(config.batch_size)
        feed_dict = {m.x_q: batch[0], m.x_a: batch[1], m.x_q_length: batch[2], m.x_a_length: batch[3], m.y_: batch[4]}
        cost, _ = session.run([m.cost, eval_op], feed_dict=feed_dict)

        costs += cost
        if (step + 1) % 10 == 0:
            print ("cost: %.5f" % cost)
    return costs/num_batches

def run_epoch_eval(session, m, data, eval_op, config, verbose=False):
    start_time = time.time()
    score = 0
    batch = data.getNext(data.size)
    if config.problemType == 'SIM':
        feed_dict={m.x_q: batch[0], m.x_a: batch[1], m.x_q_length: batch[2], m.x_a_length: batch[3], m.label: numpy.reshape(batch[4], [-1, 1])}
        score = session.run(m.pearson[0], feed_dict=feed_dict)
    else:
        feed_dict = {m.x_q: batch[0], m.x_a: batch[1], m.x_q_length: batch[2], m.x_a_length: batch[3], m.y_: batch[4]}
        score = session.run(m.accuracy, feed_dict=feed_dict)

    return score


def get_config():
    return myConfig()


def main(_):
    if not FLAGS.data_path:
        raise ValueError("Must set --data_path to data directory")

    config = get_config()
    train_data, valid_data, test_data, vocab_emb = getSickDataSet(FLAGS.data_path, type=config.problemType)

    config.num_steps_q = train_data.dataset[0].shape[1]
    config.num_steps_a = train_data.dataset[1].shape[1]
    config.vocab_size = vocab_emb.shape[0]
    config.embed_dim = vocab_emb.shape[1]

    with tf.Graph().as_default(), tf.Session() as session:

        initializer = tf.random_uniform_initializer(-config.init_scale, config.init_scale)
        with tf.variable_scope("model", reuse=None, initializer=initializer):
            m = QAModel(True, config)
        with tf.variable_scope("model", reuse=True, initializer=initializer):
            mvalid = QAModel(False, config)
            mtest = QAModel(False, config)

        tf.initialize_all_variables().run()

        for i in range(config.max_max_epoch):
            lr_decay = config.lr_decay ** max(i - config.max_epoch, 0.0)
            m.assign_lr(session, config.learning_rate * lr_decay)
            m.assign_embedding(session, vocab_emb)

            print("Epoch: %d Learning rate: %.3f" % (i + 1, session.run(m.lr)))
            avg_cost = run_epoch(session, m, train_data, m.train_op, config, verbose=True)
            print("Average cost for Epoch %d: %.5f" % (i + 1, avg_cost))
            print("Accuracy: %.4f" % run_epoch_eval(session, mvalid, valid_data, tf.no_op(), config))
            print("Accuracy: %.4f" % run_epoch_eval(session, mtest, test_data, tf.no_op(), config))

if __name__ == "__main__":
    tf.app.run()
