import numpy
import os
import tensorflow as tf
import time

from eval import map_score, precision_score, get_map_score
from readerQA import load_data

import warnings
warnings.filterwarnings("ignore")  # TODO remove


def main():
    data_dir = 'trec'
    trainData, devData, testData, qids_dev, qids_test = load_data(data_dir)

    q_max_sent_size = trainData.dataset[0].shape[1]
    a_max_sent_size = trainData.dataset[1].shape[1]

    # Load word2vec embeddings
    fname = os.path.join(data_dir, 'emb_aquaint+wiki.txt.gz.ndim=50.bin.npy')
    print "Loading word embeddings from", fname
    vocab_emb = numpy.load(fname)
    ndim = vocab_emb.shape[1]
    dummpy_word_idx = numpy.max(trainData.dataset[1])
    print "Word embedding matrix size:", vocab_emb.shape

    # Create overlap embeddings
    overlapDim = 5
    print "Generating random vocabulary for word overlap indicator features with dim:", overlapDim
    dummy_word_id = numpy.max(trainData.dataset[3])
    print "Gaussian"
    numpy_rng = numpy.random.RandomState(seed=123)
    vocab_emb_overlap = numpy_rng.randn(dummy_word_id+1, overlapDim) * 0.25
    vocab_emb_overlap[-1] = 0

    # Configuration parameters
    n_classes = 2
    n_epochs = 50
    batch_size = 1
    learning_rate = 0.005
    max_norm = 2
    filterWidth = 5
    nkernels = 100
    param_stddev = 1.0

    print 'batch_size', batch_size
    print 'n_epochs', n_epochs
    print 'learning_rate', learning_rate
    print 'max_norm', max_norm

    # Create input variables
    x_q = tf.placeholder(tf.int32, shape=[1, q_max_sent_size])
    x_a = tf.placeholder(tf.int32, shape=[1,  a_max_sent_size])
    x_overlap_q = tf.placeholder(tf.int32, shape=[1, q_max_sent_size])
    x_overlap_a = tf.placeholder(tf.int32, shape=[1, a_max_sent_size])
    y_ = tf.placeholder(tf.int32, shape=[1])

    def get_embeddings(input, paddings, embedding):
        return tf.pad(tf.nn.embedding_lookup(embedding, input), paddings, "CONSTANT")

    def get_overlap_embeddings(input, paddings, embedding):
        return tf.pad(tf.nn.embedding_lookup(embedding, input), paddings, "CONSTANT")

    paddings = [[0,0], [filterWidth-1, filterWidth-1], [0,0]]
    with tf.variable_scope("embedding_scope") as emb_scope:
        embedding = tf.get_variable("embedding", vocab_emb.shape, trainable=False)
        q_emb = tf.cast(get_embeddings(x_q, paddings, embedding), dtype=tf.float32)
        emb_scope.reuse_variables()
        a_emb = tf.cast(get_embeddings(x_a, paddings, embedding), dtype=tf.float32)

    with tf.variable_scope("overlap_embedding_scope") as overlap_emb_scope:
        overlap_embedding = tf.get_variable("overlap", vocab_emb_overlap.shape)
        q_overlap_emb = tf.cast(get_overlap_embeddings(x_overlap_q, paddings, overlap_embedding), dtype=tf.float32)
        overlap_emb_scope.reuse_variables()
        a_overlap_emb = tf.cast(get_overlap_embeddings(x_overlap_a, paddings, overlap_embedding), dtype=tf.float32)

    q_emb_concat = tf.concat(2, [q_emb, q_overlap_emb])
    a_emb_concat = tf.concat(2, [a_emb, a_overlap_emb])

    ndim += vocab_emb_overlap.shape[1]
    conv_space_Q = q_max_sent_size + 2*(filterWidth-1)
    conv_space_A = a_max_sent_size + 2*(filterWidth-1)

    # Define convolutional layer
    def conv_layer(input, filter_shape, nkernels, conv_space, stddev):
        W_conv = tf.get_variable("conv_W", filter_shape, initializer=tf.random_uniform_initializer(minval=-0.05, maxval=0.05))
        b_conv = tf.get_variable("conv_b", nkernels, initializer=tf.zeros_initializer)
        h_conv = tf.nn.relu(tf.nn.conv2d(input, filter=W_conv, strides=[1, 1, ndim, 1], padding="SAME") + b_conv)
        h_pool = tf.nn.max_pool(h_conv, ksize=[1, conv_space, 1, 1], strides=[1, conv_space, 1, 1], padding='SAME')
        return tf.reshape(h_pool, [1, nkernels])

    filter_shape = [filterWidth, ndim, 1, nkernels] # [height, width, in_channels, out_channels]
    q_emb_resized = tf.reshape(q_emb_concat, (1, conv_space_Q, ndim, 1))
    a_emb_resized = tf.reshape(a_emb_concat, (1, conv_space_A, ndim, 1))
    with tf.variable_scope("conv_scope") as conv_scope:
        h_pool_q = conv_layer(q_emb_resized, filter_shape, nkernels, conv_space_Q, param_stddev)
        conv_scope.reuse_variables()
        h_pool_a = conv_layer(a_emb_resized, filter_shape, nkernels, conv_space_A, param_stddev)

    # Combine result
    uniform_initializer = tf.random_uniform_initializer(minval=-0.05, maxval=0.05)
    with tf.variable_scope("hid_out_scope", initializer=uniform_initializer) as hidout_scope:
        M = tf.get_variable("M", [nkernels, nkernels])
        dotProd = tf.matmul(tf.matmul(h_pool_q, M), tf.transpose(h_pool_a))
        concat = tf.concat(1, [h_pool_q, dotProd, h_pool_a])

        # Fully connected linear layer (2*nkernels, 2*nkernels)
        h_w = tf.get_variable("hidden_w", [2*nkernels + 1, 2*nkernels + 1])
        h_b = tf.get_variable("hidden_b", [2*nkernels + 1])
        hid_out = tf.nn.tanh(tf.nn.xw_plus_b(concat, h_w, h_b), "hidden_out")

        # Output layer (2*nkernels, nclasses)
        output_w = tf.get_variable("output_w", [2*nkernels + 1, n_classes])
        output_b = tf.get_variable("output_b", [n_classes])
        y_conv = tf.nn.sigmoid(tf.nn.xw_plus_b(hid_out, output_w, output_b))
        y_pred = tf.nn.softmax(y_conv)
        loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(y_conv, y_))

        tf.scalar_summary(loss.op.name, loss)
        global_step = tf.Variable(0, name='global_step', trainable=False)
        prediction = tf.arg_max(y_pred, 1)

        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(loss, tvars), max_norm)
        grads_vars = zip(grads, tvars)
        optimizer = tf.train.AdamOptimizer(learning_rate)
        train_op = optimizer.apply_gradients(grads_vars, global_step=global_step)

    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())
        sess.run(tf.assign(embedding, vocab_emb))
        sess.run(tf.assign(overlap_embedding, vocab_emb_overlap))

        best_dev_map = -numpy.inf
        best_test_map = -numpy.inf
        best_test_acc = -numpy.inf

        epoch = 0
        timer_train = time.time()
        no_best_dev_update = 0
        num_train_batches = trainData.size / batch_size

        saver = tf.train.Saver()

        while epoch < n_epochs:
            timer = time.time()
            i = 0
            tot_cost = 0
            cnt = 0
            while i < num_train_batches:
                batch = trainData.getNext(batch_size)
                feed_dict = {x_q: batch[0], x_a: batch[1], x_overlap_q: batch[2], x_overlap_a: batch[3], y_: batch[4]}
                _, cost, y_pred_train = sess.run([train_op, loss, prediction], feed_dict=feed_dict)
                tot_cost += cost
                cnt += 1

                if (i+1) % 200 == 0 or i == num_train_batches:
                    y_pred_dev = numpy.zeros(devData.size)
                    for j in range(devData.size):
                        inst = devData.getNext(1)
                        feed_dict_dev = {x_q: inst[0], x_a: inst[1], x_overlap_q: inst[2], x_overlap_a: inst[3], y_: inst[4]}
                        y_pred_dev[j] = sess.run(prediction, feed_dict=feed_dict_dev)

                    dev_map = map_score(qids_dev, devData.dataset[4], y_pred_dev)
                    print('epoch: {} batch: {} train cost: {:.6f}'.format(epoch, i, tot_cost/cnt))
                    tot_cost = 0
                    cnt = 0

                    if dev_map > best_dev_map:
                        y_pred_test = numpy.zeros(testData.size)
                        for j in range(testData.size):
                            inst = testData.getNext(1)
                            feed_dict_test = {x_q: inst[0], x_a: inst[1], x_overlap_q: inst[2], x_overlap_a: inst[3], y_: inst[4]}
                            y_pred_test[j] = sess.run(prediction, feed_dict=feed_dict_test)

                        test_map = map_score(qids_test, testData.dataset[4], y_pred_test)
                        test_acc = precision_score(qids_test, testData.dataset[4], y_pred_test)
                        print('epoch: {} batch: {} dev auc: {:.4f}; test map: {:.4f}; test acc: {:.4f};'.format(epoch, i, dev_map, test_map, test_acc))

                        best_dev_map = dev_map
                        save_path = saver.save(sess, "model.ckpt", global_step=global_step)
                        print("Model saved in file: %s" % save_path)
                        no_best_dev_update = 0
                i += 1

            if no_best_dev_update >= 10:
                print "Quitting after of no update of the best score on test set", no_best_dev_update
                break

            print('epoch {} took {:.4f} seconds'.format(epoch, time.time() - timer))

            y_pred_test = numpy.zeros(testData.size)
            for j in range(testData.size):
                inst = testData.getNext(1)
                feed_dict_test = {x_q: inst[0], x_a: inst[1], x_overlap_q: inst[2], x_overlap_a: inst[3], y_: inst[4]}
                y_pred_test[j] = sess.run(prediction, feed_dict=feed_dict_test)
            get_map_score(qids_test, testData.dataset[4], y_pred_test, './eval')
            epoch += 1
            no_best_dev_update += 1

        print('Training took: {:.4f} seconds'.format(time.time() - timer_train))
        # saver.resotre(sess, "model.ckpt")
        # test_acc = map_score(qids_test, y_test, y_pred_test) * 100
        fname = os.path.join('./out', 'best_dev_params.epoch={:02d};batch={:05d};dev_acc={:.2f}.dat'.format(epoch, i, best_dev_acc))
        numpy.savetxt(os.path.join('./out', 'test.epoch={:02d};batch={:05d};dev_acc={:.2f}.predictions.npy'.format(epoch, i, best_dev_acc)), y_pred_test)

        """
        # test_prec = precision_score(qids_test, y_test, predict_prob_batch(test_set_iterator))
        # print "Test precision : " + str(test_prec)
        """

if __name__ == '__main__':
  main()
