import os
import numpy
from collections import defaultdict
import pandas
import subprocess


def map_score(qids, labels, preds):
    qid2cand = defaultdict(list)
    for qid, label, pred in zip(qids, labels, preds):
        qid2cand[qid].append((pred, label))

    average_precs = []
    for qid, candidates in qid2cand.iteritems():
        average_prec = 0
        running_correct_count = 0
        for i, (score, label) in enumerate(sorted(candidates, reverse=True), 1):
            # if label > 0:
            if label == score:
                running_correct_count += 1
                average_prec += float(running_correct_count) / i
        average_precs.append(average_prec / (running_correct_count + 1e-6))
    map_score = sum(average_precs) / len(average_precs)
    return map_score

def precision_score(qids, gold, pred):
    q_num = qids[-1]
    q_index = qids[0]

    unit_gold, unit_pred = [], []
    count_right = 0.0
    count_wrong = 0.0

    for i in range(len(qids)):
        if q_index != qids[i]:
            # do calculation
            uid = numpy.argmax(unit_pred)
            if unit_gold[uid] == 1:
                count_right += 1
            else:
                count_wrong += 1
            q_index = qids[i]

            # reset unit sets
            unit_pred, unit_gold = [], []

        unit_gold.append(gold[i])
        unit_pred.append(pred[i])

    # do the last calculation
    uid = numpy.argmax(unit_pred)
    if unit_gold[uid] == 1:
        count_right += 1
    else:
        count_wrong += 1

    return (count_right / (count_wrong + count_right) * 100)



def get_map_score(qids, gold, pred, outdir):
    N = len(pred)

    df_submission = pandas.DataFrame(index=numpy.arange(N), columns=['qid', 'iter', 'docno', 'rank', 'sim', 'run_id'])
    df_submission['qid'] = qids
    df_submission['iter'] = 0
    df_submission['docno'] = numpy.arange(N)
    df_submission['rank'] = 0
    df_submission['sim'] = pred
    df_submission['run_id'] = 'nnet'
    df_submission.to_csv(os.path.join(outdir, 'submission.txt'), header=False, index=False, sep=' ')

    df_gold = pandas.DataFrame(index=numpy.arange(N), columns=['qid', 'iter', 'docno', 'rel'])
    df_gold['qid'] = qids
    df_gold['iter'] = 0
    df_gold['docno'] = numpy.arange(N)
    df_gold['rel'] = gold
    df_gold.to_csv(os.path.join(outdir, 'gold.txt'), header=False, index=False, sep=' ')

    subprocess.call("/bin/sh run_eval.sh '{}'".format(outdir), shell=True)