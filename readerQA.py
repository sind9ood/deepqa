import numpy
import os

class dataSet():
    def __init__(self, dataset, shuffle):
        self.index = 0
        self.dataset = dataset
        self.len = len(dataset[0])
        self.shuffle = shuffle

    def getNext(self, num):
        if self.shuffle:
            index = numpy.random.randint(0, self.size)
        else: index = self.index

        if index + num > self.len:
            margin = num - (self.len - index)
            if not self.shuffle:
                self.index = margin
            return [numpy.concatenate((self.dataset[0][index:self.len], self.dataset[0][0:margin]), axis=0),
                    numpy.concatenate((self.dataset[1][index:self.len], self.dataset[1][0:margin]), axis=0),
                    numpy.concatenate((self.dataset[2][index:self.len], self.dataset[2][0:margin]), axis=0),
                    numpy.concatenate((self.dataset[3][index:self.len], self.dataset[3][0:margin]), axis=0),
                    numpy.concatenate((self.dataset[4][index:self.len], self.dataset[4][0:margin]), axis=0),
                    numpy.concatenate((self.dataset[5][index:self.len], self.dataset[5][0:margin]), axis=0),
                    numpy.concatenate((self.dataset[6][index:self.len], self.dataset[6][0:margin]), axis=0)]
        else:
            if not self.shuffle:
                self.index += num
            return [self.dataset[0][index:index+num], self.dataset[1][index:index+num],
                    self.dataset[2][index:index+num], self.dataset[3][index:index+num],
                    self.dataset[4][index:index+num], self.dataset[5][index:index+num],
                    self.dataset[6][index:index+num]]

    @property
    def size(self): return self.len


def get_seq_length(data):
    seq_len = numpy.zeros(data.shape[0], dtype=numpy.int32)
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            if data[i][j] == 56521:
                seq_len[i] = j
                break
        if seq_len[i] == 0:
            seq_len[i] = data.shape[1]
    return seq_len

def load_data(data_dir):
    q_train = numpy.load(os.path.join(data_dir, 'train.questions.npy'))
    a_train = numpy.load(os.path.join(data_dir, 'train.answers.npy'))
    q_overlap_train = numpy.load(os.path.join(data_dir, 'train.q_overlap_indices.npy'))
    a_overlap_train = numpy.load(os.path.join(data_dir, 'train.a_overlap_indices.npy'))
    y_train = numpy.load(os.path.join(data_dir, 'train.labels.npy'))

    q_dev = numpy.load(os.path.join(data_dir, 'dev.questions.npy'))
    a_dev = numpy.load(os.path.join(data_dir, 'dev.answers.npy'))
    q_overlap_dev = numpy.load(os.path.join(data_dir, 'dev.q_overlap_indices.npy'))
    a_overlap_dev = numpy.load(os.path.join(data_dir, 'dev.a_overlap_indices.npy'))
    y_dev = numpy.load(os.path.join(data_dir, 'dev.labels.npy'))
    qids_dev = numpy.load(os.path.join(data_dir, 'dev.qids.npy'))

    q_test = numpy.load(os.path.join(data_dir, 'test.questions.npy'))
    a_test = numpy.load(os.path.join(data_dir, 'test.answers.npy'))
    q_overlap_test = numpy.load(os.path.join(data_dir, 'test.q_overlap_indices.npy'))
    a_overlap_test = numpy.load(os.path.join(data_dir, 'test.a_overlap_indices.npy'))
    y_test = numpy.load(os.path.join(data_dir, 'test.labels.npy'))
    qids_test = numpy.load(os.path.join(data_dir, 'test.qids.npy'))


    q_train_length = get_seq_length(q_train)
    a_train_length = get_seq_length(a_train)
    q_dev_length = get_seq_length(q_dev)
    a_dev_length = get_seq_length(a_dev)
    q_test_length = get_seq_length(q_test)
    a_test_length = get_seq_length(a_test)

    trainData = dataSet([q_train, a_train, q_train_length, a_train_length, q_overlap_train, a_overlap_train,  y_train], shuffle=True)
    devData = dataSet([q_dev, a_dev, q_dev_length, a_dev_length, q_overlap_dev, a_overlap_dev,  y_dev], shuffle=False)
    testData = dataSet([q_test, a_test, q_test_length, a_test_length, q_overlap_test, a_overlap_test, y_test], shuffle=False)

    # Load word2vec embeddings
    fname = os.path.join(data_dir, 'emb_aquaint+wiki.txt.gz.ndim=50.bin.npy')
    vocab_emb = numpy.load(fname)
    ndim = vocab_emb.shape[1]
    # dummpy_word_idx = numpy.max(trainData.dataset[1])

    # Create overlap embeddings
    overlapDim = 5
    dummy_word_id = numpy.max(trainData.dataset[3])
    numpy_rng = numpy.random.RandomState(seed=123)
    vocab_emb_overlap = numpy_rng.randn(dummy_word_id+1, overlapDim) * 0.25
    vocab_emb_overlap[-1] = 0

    return trainData, devData, testData, qids_dev, qids_test, vocab_emb, vocab_emb_overlap